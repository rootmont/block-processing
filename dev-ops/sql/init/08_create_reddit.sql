

CREATE TABLE reddit (
  `id` int(15) NOT NULL AUTO_INCREMENT,
	`coin_id` int(15) NOT NULL UNIQUE,
	`subreddit` VARCHAR(255) NOT NULL,
	`posts` LONGTEXT NOT NULL,
	`updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)