

CREATE TABLE ranks (
  `coin_id` int(15) NOT NULL,
	`my_date` DATE NOT NULL,
	`risk_rank` float(30,15) unsigned DEFAULT NULL,
	`dev_rank` float(30,15) unsigned DEFAULT NULL,
	`social_rank` float(30,15) unsigned DEFAULT NULL,
	`usage_rank` float(30,15) unsigned DEFAULT NULL,
	`root_rank` float(30,15) unsigned DEFAULT NULL,
	`updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`coin_id`, `my_date`)
)