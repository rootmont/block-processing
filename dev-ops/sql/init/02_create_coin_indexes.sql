
USING rootmont;

CREATE INDEX idx_ ON coin(age_cluster);
CREATE INDEX idx_marketcap_cluster ON coin(marketcap_cluster);