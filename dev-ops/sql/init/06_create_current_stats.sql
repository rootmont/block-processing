
USE api;

CREATE TABLE `current_stats` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
	`coin_id` int(15) NOT NULL UNIQUE,
	`token_name` varchar(64) DEFAULT NULL,
	`price` float(30,15) unsigned DEFAULT NULL,
	`price_open` float(30,15) unsigned DEFAULT NULL,
	`volume` float(30,15) unsigned DEFAULT NULL,
	`supply` float(30,15) unsigned DEFAULT NULL,
	`marketcap` float(30,15) unsigned DEFAULT NULL,
	`updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
);
