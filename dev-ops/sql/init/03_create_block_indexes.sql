
USING rootmont;

CREATE INDEX idx_block_number ON blocks(blocknumber);
CREATE INDEX idx_my_date ON blocks(my_date);
CREATE INDEX idx_blockchain ON blocks(blockchain);