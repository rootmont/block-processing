--
-- Table structure for table `blocknumbers`
--
CREATE DATABASE IF NOT EXISTS rootmont;

USE rootmont;

DROP TABLE IF EXISTS `blocknumbers`;

CREATE TABLE `blocknumbers` (
  `blocknumber` int(30) unsigned DEFAULT NULL,
  `blockchain` varchar(256) NOT NULL,
  `my_date` date NOT NULL,
  PRIMARY KEY (`blockchain`,`my_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `blocks`
--

DROP TABLE IF EXISTS `blocks`;

CREATE TABLE `blocks` (
  `blocknumber` int(30) unsigned NOT NULL,
  `blockchain` varchar(256) NOT NULL,
  `txs` bigint(20) unsigned DEFAULT '0',
  `my_date` date DEFAULT NULL,
  `tstamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`blockchain`,`blocknumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `coin`
--

DROP TABLE IF EXISTS `coin`;

CREATE TABLE `coin` (
  `coin_id` int(15) NOT NULL AUTO_INCREMENT,
  `description` varchar(4096) DEFAULT NULL,
  `ticker_symbol` varchar(10) DEFAULT NULL,
  `company_name` varchar(30) DEFAULT NULL,
  `token_name` varchar(30) DEFAULT NULL,
  `ico` int(1) unsigned DEFAULT NULL,
  `mineable` int(1) unsigned DEFAULT NULL,
  `open_source` int(1) unsigned DEFAULT NULL,
  `ico_raise` int(30) unsigned DEFAULT NULL,
  `max_supply` bigint(20) DEFAULT NULL,
  `hash_algorithm` varchar(32) DEFAULT NULL,
  `blockchain_type` varchar(32) DEFAULT NULL,
  `execution` int(15) unsigned DEFAULT NULL,
  `potential` int(15) unsigned DEFAULT NULL,
  `marketcap_cluster` varchar(15) DEFAULT NULL,
  `age_cluster` varchar(15) DEFAULT NULL,
  `disclosure` int(1) unsigned DEFAULT NULL,
  `website_url` varchar(256) DEFAULT NULL,
  `whitepaper_url` varchar(256) DEFAULT NULL,
  `twitter` varchar(128) DEFAULT NULL,
  `reddit` varchar(128) DEFAULT NULL,
  `telegram` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`coin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=456 DEFAULT CHARSET=latin1;


--
-- Table structure for table `coin_category`
--

DROP TABLE IF EXISTS `coin_category`;

CREATE TABLE `coin_category` (
  `coin_id` int(15) NOT NULL,
  `category` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`coin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `timeseries`
--

DROP TABLE IF EXISTS `timeseries`;

CREATE TABLE `timeseries` (
  `coin_id` int(15) NOT NULL,
  `my_date` date NOT NULL,
  `price_high` float(30,15) unsigned DEFAULT '0.000000000000000',
  `price_low` float(30,15) unsigned DEFAULT '0.000000000000000',
  `price_open` float(30,15) unsigned DEFAULT '0.000000000000000',
  `price_close` float(30,15) unsigned DEFAULT '0.000000000000000',
  `trading_volume` bigint(20) unsigned DEFAULT '0',
  `daily_transactions` bigint(20) unsigned DEFAULT NULL,
  `marketcap` bigint(20) unsigned DEFAULT '0',
  `supply` bigint(20) unsigned DEFAULT '0',
  PRIMARY KEY (`coin_id`,`my_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `benchmarks`
--

DROP TABLE IF EXISTS `benchmarks`;

CREATE TABLE `benchmarks` (
  `my_date` date NOT NULL,
  `cluster_type` varchar(64) NOT NULL,
  `cluster_name` varchar(64),
  `metric` varchar(64) NOT NULL,
  `mean` float(30,15) DEFAULT 0,
  `std` float(30,15) DEFAULT 0,
  `min` float(30,15) DEFAULT 0,
  `25%` float(30,15) DEFAULT 0,
  `50%`float(30,15) DEFAULT 0,
  `75%` float(30,15) DEFAULT 0,
  `max` float(30,15) DEFAULT 0,
  `count` int(15) DEFAULT 0,
  PRIMARY KEY (`my_date`, `cluster_name`, `metric`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `timeseries_old`
--

DROP TABLE IF EXISTS `timeseries_old`;

CREATE TABLE `timeseries_old` (
  `coin_id` int(15) NOT NULL,
  `my_date` date NOT NULL,
  `price_high` float(30,15) unsigned DEFAULT '0.000000000000000',
  `price_low` float(30,15) unsigned DEFAULT '0.000000000000000',
  `price_open` float(30,15) unsigned DEFAULT '0.000000000000000',
  `price_close` float(30,15) unsigned DEFAULT '0.000000000000000',
  `trading_volume` bigint(20) unsigned DEFAULT '0',
  `daily_transactions` bigint(20) unsigned DEFAULT '0',
  `marketcap` bigint(20) unsigned DEFAULT '0',
  `supply` bigint(20) unsigned DEFAULT '0',
  `upside_30` float(30,15) unsigned DEFAULT '0.000000000000000',
  `downside_30` float(30,15) unsigned DEFAULT '0.000000000000000',
  `metcalfe_exponent_30` float(30,15) unsigned DEFAULT '0.000000000000000',
  `metcalfe_correlation_30` float(30,15) unsigned DEFAULT '0.000000000000000',
  `metcalfe_proportion_30` float(30,15) unsigned DEFAULT '0.000000000000000',
  PRIMARY KEY (`coin_id`,`my_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


