

CREATE TABLE twitter (
  `created_at` DATE NOT NULL,
  `updated_at` DATE NOT NULL,
  `deleted_at` DATE,
  `id` INT(15) UNSIGNED NOT NULL,
  `ticker_dolar` VARCHAR(10) NOT NULL,
  `ticker` VARCHAR(10) NOT NULL,
  `company` VARCHAR(256) NOT NULL,
  `coin` VARCHAR(10) NOT NULL,
  `likes` INT(15) UNSIGNED NOT NULL,
  `replies` INT(15) UNSIGNED NOT NULL,
  `retweets` INT(15) UNSIGNED NOT NULL,
  `screen_name` VARCHAR(256) NOT NULL,
  `tweet_created_at` DATE NOT NULL,
  `tweet_id` VARCHAR(256) NOT NULL,
  `source_id` VARCHAR(10) NOT NULL,
  `text` VARCHAR(256) NOT NULL,
  `user` VARCHAR(256) NOT NULL,
  PRIMARY KEY (`id`)
)
