use rootmont;

CREATE FULLTEXT INDEX fidx_token_name ON coin(token_name);
CREATE INDEX idx_coin_id ON coin(coin_id);
CREATE INDEX idx_coin_id ON timeseries(coin_id);
CREATE INDEX idx_my_date ON timeseries(my_date);
CREATE INDEX idx_price_close ON timeseries(price_close);
CREATE INDEX idx_daily_transactions ON timeseries(daily_transactions);
CREATE INDEX idx_supply ON timeseries(supply);
CREATE INDEX idx_marketcap ON timeseries(marketcap);
