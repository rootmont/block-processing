import pytest
import datetime
from typing import List

from gathering.models.github_models import *

from gathering.sensors.github import _get_last_identifier, _is_github_org, _historical_stars, \
    _get_repo_id, get_github_data, _historical_forks, _historical_issues, \
    _historical_commits, _historical_comments, _accumulate_historical_data, backfill_github_data

repository_pages = [
    "https://github.com/Firstbloodio/token",
    "https://github.com/ArcBlock/ArcBlock-Token",
    "https://github.com/Achain-Dev/Achain",
    "https://github.com/input-output-hk/cardano-sl/",
    "https://github.com/AdBank/adbank-token-management-system",
    "https://github.com/AdExBlockchain/adex-core",
]

project_pages = [
    "https://github.com/ambrosus",
]


def test_backfill_github_data():
    r1 = backfill_github_data(20, repository_pages[0])
    assert r1 is not None
    r2 = backfill_github_data(20, repository_pages[0])
    assert r1.__eq__(r2), "We should have same size after going through same repository"

"""
def test__accumulate_historical_data():
    r = _accumulate_historical_data(20, project_pages[0])
    assert r is not None
    assert r.forks > 0
    assert r.commits > 0
    assert r.project_repo_url == project_pages[0]
    assert r.stars > 0

def test__githubdata():
    date_list: List[datetime.datetime] = [
        _parse_github_date("2017-01-03T12:11:32"),
        _parse_github_date("2017-01-04T12:11:32"),
        _parse_github_date("2017-01-05T12:11:32"),
        _parse_github_date("2017-01-06T12:11:32"),
        _parse_github_date("2017-01-07T12:11:32"),
    ]

    # What happens when we have something that is open and closed on the same day?
    closed_issue_dates = [
        (_parse_github_date("2017-01-03T12:11:32"), _parse_github_date("2017-01-03T12:12:32")),
        (_parse_github_date("2017-01-03T12:11:32"), _parse_github_date("2017-01-06T12:11:32")),
        (_parse_github_date("2017-01-04T12:11:32"), _parse_github_date("2017-01-06T12:11:32")),
        (_parse_github_date("2017-01-05T12:11:32"), _parse_github_date("2017-01-06T12:11:32")),
        (_parse_github_date("2017-01-06T12:11:32"), None),
    ]

    names_dates = [
        ("noodles", _parse_github_date("2017-01-03T12:11:32")),
        ("jamoli", _parse_github_date("2017-01-04T12:11:32")),
        ("jamoli", _parse_github_date("2017-01-05T12:11:32")),
        ("bruce", _parse_github_date("2017-01-06T12:11:32")),
        ("nar", _parse_github_date("2017-01-07T12:11:32")),
    ]

    # These get sorted desc when they are put into the GithubData Object
    issues = [GithubIssue(k[0], k[1]) for k in closed_issue_dates]
    commits = [GithubCommit(x[0], x[1]) for x in names_dates]
    comments = [GithubComment(x[0], x[1]) for x in names_dates]

    g = GithubData("http://goose.com", date_list, date_list, issues, commits, comments)
    assert g.open_issues_before(datetime.datetime.now()) == 1, "Should be one open issue right now"
    assert g.open_issues_before(_parse_github_date("2017-01-05T12:11:32")) == 2, "Should be two open issues after 2017-01-05T12:11:32"
    assert g.stars_before(_parse_github_date("2017-01-06T12:11:32")) == 3, "Should have three stars before 2017-01-06T12:11:32"
    assert g.commits_before(_parse_github_date("2017-01-06T12:11:32")) == 3, "Should have 3 commits before 2017-01-06T12:11:32"
    assert g.forks_before(_parse_github_date("2017-01-06T12:11:32")) == 3, "Should be 3 forks before 2017-1-06"
    assert g.open_issues_before(_parse_github_date("2017-01-07T12:11:32")) == 1, "Should only be one open issue after 1-07"
    assert g.open_issues_before(_parse_github_date("2017-01-04T12:11:32")) == 1, "Should only be one open issue during 1-04"
    assert g.unique_contributors_before(_parse_github_date("2017-01-06T12:11:32")) == 2, "Should be two unique contributors before 1-06"
    assert g.last_commit_before(_parse_github_date("2017-01-06T12:11:32")) == commits[2]
    assert g.last_commit_before(_parse_github_date("2017-01-07T12:11:32")) == commits[1]


def test__get_last_identifier():
    assert "goose" == _get_last_identifier("buce.com/geese/mice/goose"), "should be last path of url, goose"
    assert "jimini" == _get_last_identifier("ok.com/jimini"), "should be last path of url, jimini"


def test__is_github_org():
    for url in repository_pages:
        assert _is_github_org(url) is False, "{} should be a repository page".format(url)
    for url in project_pages:
        assert _is_github_org(url) is True, "{} should be an organization".format(url)


def test__get_repo_id():
    url = repository_pages[0]
    repository_id = _get_repo_id(url)
    assert repository_id is not None, "{} should have a repository id".format(url)
    assert type(repository_id) is int, "Should have an int repository id"


def test__historical_stars():
    for page in repository_pages:
        star_history = _historical_stars(page)
        github_data = get_github_data(page)
        num_stargazers = github_data["stargazers"]
        assert num_stargazers == len(star_history), "Stargazers amount {} should be the length of star history {}".format(num_stargazers, len(star_history))


def test__historical_forks():
    for page in repository_pages:
        fork_history = _historical_forks(page)
        data = get_github_data(page)
        assert data["forks"] == len(fork_history), "Forks amount {} should be the length of forks history {} for {}".format(data["forks"], len(fork_history), page)


def test_github_issue():
    created_str = "2017-01-04T12:11:32"
    closed_by_str = "2018-01-04T12:23:12"
    created = _parse_github_date(created_str)
    closed = _parse_github_date(closed_by_str)

    after_created = _parse_github_date("2017-01-05T12:11:32")
    same_created = _parse_github_date("2017-01-04T12:11:32")
    before_created = _parse_github_date("2017-01-03T12:11:32")
    after_closed = _parse_github_date("2018-01-05T12:23:12")
    same_closed = _parse_github_date("2018-01-04T12:23:12")
    before_closed = _parse_github_date("2018-01-03T12:23:12")

    # Make sure we can create these objects by string and datetime.datetime
    g1 = GithubIssue(created_str, closed_by_str)
    g2 = GithubIssue(created, closed)
    g3 = GithubIssue(created, None)

    # Make sure time relations are ok for datetime.datetime
    assert g1.created_before(after_created)
    assert not g1.created_before(same_created)
    assert not g1.created_before(before_created)

    assert g1.closed_before(after_closed)
    assert not g1.closed_before(same_closed)
    assert not g1.closed_before(before_closed)

    # Make sure time relations are ok for datetime.date
    assert g1.created_before(after_created.date()), "Should be created before date in the future"
    assert not g1.created_before(same_created.date()), "Should not be created before same date"
    assert not g1.created_before(before_created.date()), "Should not be created before date in the past"

    # Make sure if the issue isn't closed we handle that case
    assert not g3.closed_before(datetime.datetime.now()), "Should not be closed if still open"


def test__historical_issues():
    for page in repository_pages:
        issues: List[GithubIssue] = _historical_issues(page)
        data = get_github_data(page)
        open_issues = list(filter(lambda x: x.created_before(datetime.datetime.now()) and not x.closed_before(datetime.datetime.now()), issues))
        assert data["open_issues"] == len(open_issues), "Issues amount {} should be length of filtered issues {}".format(data["open_issues"], len(open_issues))


def test__historical_commits():
    commits: List[GithubCommit] = _historical_commits(repository_pages[0])
    comments: List[GithubComment] = _historical_comments(repository_pages[0])
    data = get_github_data(repository_pages[0])
    assert data["contributions"] == len(commits) + len(comments), "Number of contributions {} should equal number of commits {} for {}".format(data["contributions"], len(commits) + len(comments), repository_pages[0])
"""
