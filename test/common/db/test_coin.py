from common.db.coin import get_my_date_for_offset_range, get_timeseries_records_count


def test_get_timeseries_records_count():
    num_records = get_timeseries_records_count(1)
    assert num_records is not None
    assert type(num_records) is int


def test_get_my_date_for_offset_range():
    coin_id = 1
    num_records = get_timeseries_records_count(coin_id)
    offset = 50
    for i in range(0, num_records, offset):
        records = get_my_date_for_offset_range(coin_id, i, offset)
        assert records is not None
        assert type(records) is list
        assert len(records) > 0
