import datetime
from common.db.blocks import *

block_chain = "test"
now = datetime.datetime.now()
three_days_ago = now - datetime.timedelta(days=3)
five_days_ago = now - datetime.timedelta(days=5)
num_transactions = 5
block_number = 1


def insert_one_block():
    """
    Inserts a block three days ago for a test blockchain
    :return: None
    """
    insert_block(block_number, block_chain, three_days_ago, num_transactions)
    blocks = get_blocks(block_number, block_number, block_chain)
    assert blocks.__len__() == 1, "Should be one block"


def remove_one_block():
    """
    Deletes a block three days ago for a test blockchain
    :return: None
    """
    delete_block_row("test", block_number)
    blocks = get_blocks(block_number, block_number, block_chain)
    assert blocks.__len__() == 0, "Should be zero blocks"


def test_blocknums_by_date():
    insert_one_block()
    blocks, last_date = get_blocknums_by_date(block_chain, five_days_ago)
    assert blocks.__len__() == 1, "Length of block dict should be one"
    remove_one_block()


def test_get_blocknums_by_date():
    insert_one_block()
    blocks, last_date = get_blocknums_by_date(block_chain, five_days_ago)
    assert blocks.__len__() == 1, "Length of block numbers should be one"
    remove_one_block()


def test_get_latest_block():
    insert_one_block()
    bn = get_latest_block(block_chain)
    assert bn == block_number, "Blocknumber should be 1"
    remove_one_block()


def test_get_blocks_from_date():
    insert_one_block()
    df = get_blocks_from_date(five_days_ago, block_chain)
    assert df is not None
    remove_one_block()
