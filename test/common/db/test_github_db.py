import datetime
from typing import List

from common.db.github_db import insert_for_github_data, get_for_coin_id
from gathering.sensors.github import GithubIssue, GithubCommit, GithubComment, GithubData
from common.db.github_db import parse_github_date

def test_insert_for_coin_id():
    date_list: List[datetime.datetime] = [
        parse_github_date("2017-01-03T12:11:32"),
        parse_github_date("2017-01-04T12:11:32"),
        parse_github_date("2017-01-05T12:11:32"),
        parse_github_date("2017-01-06T12:11:32"),
        parse_github_date("2017-01-07T12:11:32"),
    ]

    # What happens when we have something that is open and closed on the same day?
    closed_issue_dates = [
        (parse_github_date("2017-01-03T12:11:32"), parse_github_date("2017-01-03T12:12:32")),
        (parse_github_date("2017-01-03T12:11:32"), parse_github_date("2017-01-06T12:11:32")),
        (parse_github_date("2017-01-04T12:11:32"), parse_github_date("2017-01-06T12:11:32")),
        (parse_github_date("2017-01-05T12:11:32"), parse_github_date("2017-01-06T12:11:32")),
        (parse_github_date("2017-01-06T12:11:32"), None),
    ]

    names_dates = [
        ("noodles", parse_github_date("2017-01-03T12:11:32")),
        ("jamoli", parse_github_date("2017-01-04T12:11:32")),
        ("jamoli", parse_github_date("2017-01-05T12:11:32")),
        ("bruce", parse_github_date("2017-01-06T12:11:32")),
        ("nar", parse_github_date("2017-01-07T12:11:32")),
    ]

    # These get sorted desc when they are put into the GithubData Object
    issues = [GithubIssue(k[0], k[1]) for k in closed_issue_dates]
    commits = [GithubCommit(x[0], x[1]) for x in names_dates]
    comments = [GithubComment(x[0], x[1]) for x in names_dates]

    g = GithubData(20, "http://goose.com", date_list, date_list, issues, commits, comments)

    insert_for_github_data(g)


def test_get_for_coin_id():
    g = get_for_coin_id(20)
    assert g is not None
