from common.db.reddit_db import get_coin_id_for_reddit


def test_get_coin_id_for_reddit():
    coin_id = get_coin_id_for_reddit("bitcoin")
    assert coin_id is not None, "coin_id should not be None"
