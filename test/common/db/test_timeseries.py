import pytest
import timeit
from common.db.timeseries import *


def test_get_recent_transactions_default_days_back():
    print("\n\n\n-------------------------\n\n\n")
    start = timeit.timeit()
    transactions = get_recent_transactions()
    assert transactions is not None
    end = timeit.timeit()
    print(end - start)


def test_get_recent_transactions_7_days_back():
    print("\n\n\n-------------------------\n\n\n")
    start = timeit.timeit()
    transactions = get_recent_transactions(end_date=datetime.date.today(), days_back=7)
    assert transactions is not None
    end = timeit.timeit()
    print(end - start)


def test_get_recent_transactions_180_days_back():
    print("\n\n\n-------------------------\n\n\n")
    start = timeit.timeit()
    transactions = get_recent_transactions(end_date=datetime.date.today(), days_back=180)
    assert transactions is not None
    end = timeit.timeit()
    print(end - start)

""" 
def test_get_ntxs_time():
    print("\n\n\n-------------------------\n\n\n")
    syms = ['BAT', 'DOGE', 'BCH']
    start = timeit.timeit()
    print(get_ntxs(syms))
    end = timeit.timeit()
    print(end - start)
"""

"""
def test_get_updown_time():
    print("\n\n\n-------------------------\n\n\n")
    syms = ['BAT', 'DOGE', 'BCH']
    start = timeit.timeit()
    print(get_updown(syms))
    end = timeit.timeit()
    print(end - start)


def test_get_latest_marketcap_time():
    print("\n\n\n-------------------------\n\n\n")
    start = timeit.timeit()
    print(get_latest_marketcaps())
    end = timeit.timeit()
    print(end - start)


def test_get_metcalfe():
    print("\n\n\n-------------------------\n\n\n")
    syms = ['BAT', 'DOGE', 'BCH']
    start = timeit.timeit()
    print(get_metcalfe(syms))
    end = timeit.timeit()
    print(end - start)


def test_get_latest_supplies():
    print("\n\n\n-------------------------\n\n\n")
    start = timeit.timeit()
    print(get_latest_supplies())
    end = timeit.timeit()
    print(end - start)


def test_get_all_risk_data():
    print("\n\n\n-------------------------\n\n\n")
    start = timeit.timeit()
    print(get_all_risk_inputs())
    end = timeit.timeit()
    print(end - start)



def test_get_all_price_close():
    print("\n\n\n-------------------------\n\n\n")
    syms = ['BTC', 'BAT', 'DOGE', 'BCH']
    start = timeit.timeit()
    print(get_all_price_close(syms))
    end = timeit.timeit()
    print(end - start)
"""
