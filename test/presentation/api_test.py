#!/usr/bin/python3
import requests
import json
import sys
import coloredlogs
import random
import time
from common.db import coin
import logging
from multiprocessing import Pool

clear_cache = '?clear_cache=1'

logger = logging.getLogger('api test')
url = None

sample_survey = {
    'email': 'info@rootmont.com',
    'risk': 5,
    'price': 5,
    'updown': 3,
    'dev': 4,
    'social': 2,
    'usage': 5,
    'industries': ['Privacy', 'Platform', 'Payments'],
    'marketcaps': ['Large Cap', 'Medium Cap'],
    'ages': ['Emerging', 'Established', 'Mature']
}

coloredlogs.install()

# test all apis for status codes and correct json

def get_random_coins(num):
    all_coins = coin.get_all_coin_info()
    #all_coins = json.loads(requests.get(url + '/all-coins').text)
    return random.choices(all_coins, k=num)


def check_status_post(url, payload):
    #maybe_print('Checking path %s' % url)
    start = time.time()
    response = requests.post(url, json=payload)
    end = time.time()
    logger.info('Path %s took %s' % (url, end - start))
    if response.status_code != requests.codes.ALL_GOOD:
        logger.error('PATH RETURNED BAD STATUS: %s %s' % (url,response.status_code))
        return False, response
    else:
        #maybe_print('PATH RETURNED GOOD STATUS: ' + path)
        return check_json(response), response



def check_status_get(url):
    #maybe_print('Checking path %s' % url)
    start = time.time()
    response = requests.get(url)
    end = time.time()
    logger.info('Path %s took %s' % (url, end - start))
    if response.status_code != requests.codes.ALL_GOOD:
        logger.error('PATH RETURNED BAD STATUS: %s %s' % (url,response.status_code))
        return False, response
    else:
        #maybe_print('PATH RETURNED GOOD STATUS: ' + path)
        return check_json(response), response


def check_json(response):
    try:
        j = json.loads(response.text)
        if 'NaN' in response.text: raise json.JSONDecodeError('','',0)
        if j is None or len(j) == 0: raise ValueError()
        #maybe_print('PATH RETURNED GOOD JSON: ' + path)
        return True
    except json.JSONDecodeError:
        logger.error('%s returned bad json' % response.url)
        return False
    except ValueError:
        logger.error('{} returned empty json: {}'.format(response.url, j))
        return False

def check_catalog():
    paths = [
        '/catalog/global'
    ]
    categories = json.loads(requests.get(url + '/all-categories').text)
    for cat_type in categories:
        for cat in categories[cat_type]:
            paths.append('/catalog/%s/%s' % (cat_type, cat))

    logger.info('Checking catalog paths')
    for path in paths:
        full_url = url + path
        check_status_get(full_url)
        full_url +=  clear_cache
        check_status_get(full_url)

def check_benchmarks():
    paths = [
        '/benchmark/marketcap',
        '/benchmark/age',
        '/benchmark/industry'
    ]
    logger.info('Checking benchmark paths')
    for path in paths:
        full_url = url + path + clear_cache
        check_status_get(full_url)


def check_coin_path(coin):
    symbol = coin['ticker_symbol']
    full_url = url + '/coin-report/' + symbol + clear_cache
    good, response = check_status_get(full_url)
    check_json(response)
    full_url = url + '/coin-report/' + symbol
    check_status_get(full_url)


def check_coins():
    logger.info('Checking coin paths')
    all_coins = json.loads(requests.get(url + '/all-coins').text)
    for coin in all_coins:
        check_coin_path(coin)


def check_dashboard():
    logger.info('Checking master table')
    check_status_get(url + '/master-table/all')
    chosen = get_random_coins(10)
    coins = json.dumps([x['token_name'] for x in chosen])
    good, response = check_status_get(url + '/master-table/columns')
    if good:
        all_cols = json.loads(response.text)
        chosen = random.choices(all_cols, k=10)
        cols = json.dumps(chosen)
        check_status_get(url + '/master-table/?cols=%s&rows=%s' % (cols, coins))

    paths = ['ntx', 'vol', 'mcap', 'price']
    symbols = '?symbols=' + ','.join([x['ticker_symbol'] for x in get_random_coins(10)])
    for path in paths:
        check_status_get(url + '/' + path + symbols)

    num_movers = 5
    day_back = 90
    check_status_get(url + '/movers/%d/%d' % (day_back, num_movers))


def check_survey():
    full_url = url + '/suggestions/'
    check_status_post(full_url, sample_survey)


def check_timeseries():
    paths = ['ntx','price','mcap','vol']
    chosen = get_random_coins(10)
    coins = ','.join([x['ticker_symbol'] for x in chosen])
    for path in paths:
        full_url = '%s/%s?symbols=%s' % (url, path, coins)
        check_status_get(full_url)


def check_logos():
    logger.info('Checking logo paths')
    all_coins = json.loads(requests.get(url + '/all-coins').text)
    for coin in all_coins:
        symbol = coin['ticker_symbol']
        path = '/logo/symbol/' + symbol
        full_url = url + path
        check_status_get(full_url)


def check_prices():
    logger.info('Checking prices')
    path = '/prices'
    full_url = url + path
    check_status_get(full_url)
    coins = get_random_coins(10)
    for coin in coins:
        name = coin['token_name']
        path = '/current-stats/%s' % name
        full_url = url + path
        check_status_get(full_url)


def check_backtests():
    logger.info('Checking backtest paths')
    full_url = '%s/backtest/strategies' % (url)
    check_status_get(full_url)

    filter = \
        [
            {
                'metric': 'daily_transactions',
                'days back': 90,
                'percentile': False,
                'min': 100
            },
            {
                'metric': 'price_open',
                'percentile': False,
                'min': 0.25,
                'max': 0.75
            },
            {
                'age': ["Established","Emerging"]
            }
        ]
    full_url = '%s/backtest/filter/' % (url)
    check_status_post(full_url, filter)

    # full_url = '%s/backtest/monthly/' % (url)
    # check_status_post(full_url, filter)
    #
    # full_url = '%s/backtest/yearly/' % (url)
    # check_status_post(full_url, filter)

    # full_url = '%s/backtest/daily/' % (url)
    # check_status_post(full_url, filter)
    #
    # full_url = '%s/backtest/weekly/' % (url)
    # check_status_post(full_url, filter)

    full_url = '%s/backtest/' % (url)
    check_status_post(full_url, filter)

    full_url = '%s/backtest/filter-list/' % (url)
    check_status_get(full_url)


def check_charts():
    logger.info('Checking chart paths')
    chosen = get_random_coins(10)
    symbols = [x['ticker_symbol'] for x in chosen]
    for symbol in symbols:
        full_url = '%s/metcalfe-charts/%s' % (url, symbol)
        check_status_get(full_url)
        full_url = '%s/price-charts/%s' % (url, symbol)
        check_status_get(full_url)


def parallel_f(func):
    return func()


def run_full_api_test(api_type, parallel=False):
    global url
    if api_type == 'local':
        url = 'http://localhost:5001'
    elif api_type == 'dev':
        url = 'https://dev.rootmont.io'
    elif api_type == 'staging':
        url = 'https://staging.rootmont.io'
    elif api_type == 'prod':
        url = 'https://rootmont.io'
    else:
        logger.info('Unknown api type: %s' % api_type)

    logger.info('testing %s with parallel=%s' % (url, parallel))

    if parallel:
        fxns = [
            check_catalog,
            check_prices,
            check_charts,
            check_backtests,
            check_benchmarks,
            check_dashboard,
            check_timeseries,
            check_coins,
            check_survey,
        ]
        with Pool(8) as p:
            p.map(parallel_f, fxns)
    else:
        check_dashboard()
        check_catalog()
        check_prices()
        check_charts()
        check_timeseries()
        check_backtests()
        check_survey()
        check_benchmarks()
        check_coins()
        # check_logos()


if __name__ == "__main__":
    if len(sys.argv) < 2:
        api_type = 'local'
    else:
        api_type = sys.argv[1]
    if len(sys.argv) < 3:
        parallel = False
    else:
        parallel = True
    run_full_api_test(api_type, parallel)

