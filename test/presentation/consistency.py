import requests
import json
import sys
import coloredlogs

url = ''
import logging
logger = logging.getLogger('consistency test')
clear_cache = '?clear_cache=1'
coloredlogs.install()


def get_catalog():
    path = '/catalog/global'
    full_url = url + path + clear_cache
    response = requests.get(full_url)
    catalog = json.loads(response.text)
    return catalog


def get_benchmarks():
    paths = [
        'marketcap',
        'age',
        'industry'
    ]
    benchmarks = {}
    for path in paths:
        full_url = url + '/benchmark/' + path + clear_cache
        response = requests.get(full_url)
        benchmarks[path] = json.loads(response.text)
    return benchmarks


def check_internal_consistency_coin_reports():
    catalog = get_catalog()
    symbols = [x['symbol'] for x in catalog]
    for symbol in symbols:
        full_url = url + '/coin-report/' + symbol + clear_cache
        response = requests.get(full_url)
        j = json.loads(response.text)
        for metric_type in ['social', 'development', 'usage', 'price performance']:
            metrics = j[metric_type]
            avg_pctile = sum([metrics[x]['percentile'] for x in metrics if type(metrics[x]) == dict]) / (len(metrics) - 1)
            if round(metrics['percentile'], 2) != round(avg_pctile, 2):
                logger.error('Coin report for {} has inconsistent scores for {} stats: {} != {}'.format(symbol, metric_type, metrics['percentile'], avg_pctile))
    logger.info('Coin report consistency check complete')

def check_internal_consistency_catalog():
    catalog = get_catalog()
    for row in catalog:
        actual_root_rank = row['overall percentile']
        expected_root_rank = row['social percentile'] + row['development percentile'] + row['price percentile'] + row['usage percentile']
        expected_root_rank /= 4
        if round(actual_root_rank, 2) != round(expected_root_rank, 2):
            logger.error('Catalog row {} inconsistent, root rank was {}, should be {}'.format(row['name'], actual_root_rank, expected_root_rank))
    logger.info('Catalog consistency check complete')

def check_internal_consistency_benchmarks():
    benchmarks = get_benchmarks()
    catalog = get_catalog()
    for btype, benchmark in benchmarks.items():
        for metric_obj in benchmark:
            metric = list(metric_obj.keys())[0]
            # metric_obj = {'metric': [segments]}
            counts = {
                k: v['count']
                for segment in metric_obj[metric]
                for k, v in segment.items()
            }
            sum_non_global = sum([v for k,v in counts.items() if k != 'Global'])
            if counts['Global'] != sum_non_global:
                msg = 'Benchmark type %s for metric %s has mismatching global count %d with sum of segments %d' % (btype, metric, counts['Global'], sum_non_global)
                logger.error(msg)
            if counts['Global'] != len(catalog):
                msg = 'Benchmark type %s for metric %s has mismatching global count %d different from catalog count %d' % (btype, metric, counts['Global'], len(catalog))
                logger.error(msg)
            maximums = {
                k: v['max']
                for segment in metric_obj[metric]
                for k, v in segment.items()
                if 'max' in v
            }
            max_non_global = max([v for k,v in maximums.items() if k != 'Global'])
            if 'Global' not in maximums:
                msg = 'Empty benchmark? %s' % metric_obj
                logger.error(msg)
            elif maximums['Global'] != max_non_global:
                msg = 'Benchmark type %s for metric %s has mismatching global maximum %s different from segment maximum %s' % (btype, metric, maximums['Global'], max_non_global)
                logger.error(msg)
    logger.info('Benchmark consistency check complete')


def check_consistency(api_type):
    global url
    if api_type == 'local':
        url = 'http://localhost:5001'
    elif api_type == 'dev':
        url = 'http://dev.rootmont.io'
    elif api_type == 'staging':
        url = 'http://staging.rootmont.io'
    elif api_type == 'prod':
        url = 'https://rootmont.io'
    else:
        logger.info('Unknown api type: %s' % api_type)

    logger.info('testing %s for consistency' % url)
    check_internal_consistency_coin_reports()
    check_internal_consistency_benchmarks()
    check_internal_consistency_catalog()


if __name__ == "__main__":
    if len(sys.argv) < 2:
        api_type = 'local'
    else:
        api_type = sys.argv[1]
    check_consistency(api_type)

