import os
import argparse
import yaml
import logging
from time import time

from common.config import format_logger
from common.env import SENDGRID_API_KEY, SENDGRID_DEFAULT_FROM
from flask import Flask, request
from flask_caching import Cache
# from flask_sendgrid import SendGrid
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

logger = logging.getLogger('presentation')
format_logger(logger)

class Application:
    def __init__(self):

        # current directory path
        dir_path = os.path.dirname(os.path.realpath(__file__))

        parser = argparse.ArgumentParser(description='Service to gather information regarding coins')
        parser.add_argument('-p', default=False, type=lambda x: (str(x).lower() == 'true'))
        args = parser.parse_args()

        self._config = None
        if args.p is not None and args.p:
            self._config = yaml.safe_load(open(os.path.join(dir_path, "config-prod.yml")))
        else:
            self._config = yaml.safe_load(open(os.path.join(dir_path, "config-dev.yml")))

        self._app = Flask(__name__, static_url_path="", template_folder='./templates')

        self._app.config['SENDGRID_API_KEY'] = SENDGRID_API_KEY
        self._app.config['SENDGRID_DEFAULT_FROM'] = SENDGRID_DEFAULT_FROM

        self._app.debug = bool(self._config['debug'])
        self._app.name = 'presentation'

        self._cache = Cache(self._app, config={'CACHE_TYPE': 'simple'})
        # self._mail = SendGrid(self._app)
        self.mail_client = SendGridAPIClient(SENDGRID_API_KEY)


    def send_email(self, to_email, subject, from_email=None, html=None):
        if not from_email:
            raise ValueError("Missing from email.")
        if not html:
            raise ValueError("Missing html or text.")


        try:

            message = Mail(
                from_email=from_email,
                to_emails=to_email,
                subject=subject,
                html_content=html,
                plain_text_content=html)
            logger.info('Sending email to {}'.format(to_email))
            response = self.mail_client.send(message)
            logger.debug('Sendgrid responded with {}, {}, {}'.format(response.status_code, response.body, response.headers))
        except Exception as e:
            logger.error(e)

    def run(self):
        self._app.run(host=str(self._config['host']), port=int(self._config['port']))

    @property
    def cache(self):
        return self._cache

    @property
    def mail(self):
        return self._mail

    @property
    def flask_app(self):
        return self._app


application = Application()


@application.flask_app.before_request
def before_request():
    request.start_time = time()
    if request.args.get('clear_cache') is not None:
        logger.info('Clearing cache for %s' % (application.flask_app.view_functions[request.endpoint]))
        if len(request.view_args) > 0:
            arg = request.view_args.values().__iter__().__next__()
            application.cache.delete_memoized(application.flask_app.view_functions[request.endpoint], arg)
        else:
            application.cache.delete_memoized(application.flask_app.view_functions[request.endpoint])


@application.flask_app.after_request
def after_request(response):
    logger.info('Request %s took %f seconds' % (request, time() - request.start_time))
    return response
