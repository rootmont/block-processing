import pandas as pd
import logging

from common.db import daily_stats
from common.config import format_logger

logger = logging.getLogger('vectors')
format_logger(logger)



def get_coins_for_survey(survey):
    survey_distribution = unpack_survey(survey)
    coin_vectors = get_coin_vectors()
    ordered = sort_coins_by_survey(survey_distribution, coin_vectors)
    return ordered.iloc[-5:]


# coin_vector = [risk, industry-one-hot, marketcaps-one-hot, age-one-hot, dev-pctile, social-pctile, usage-pctile, up/down-pctile]
def get_coin_vectors():
    vectors = daily_stats.get_coin_vectors()
    df = pd.DataFrame(vectors, columns=['name', 'risk', 'industry', 'marketcap', 'age', 'dev', 'social', 'usage', 'price', 'updown'], index=[x['name'] for x in vectors])
    one_hot = pd.get_dummies(df, columns=['industry', 'marketcap', 'age'])
    one_hot['industry'] = df['industry']
    one_hot['marketcap'] = df['marketcap']
    one_hot['age'] = df['age']

    return one_hot


def sort_coins_by_survey(survey, coin_vectors):
    coin_vectors['rank'] = 0
    for k,v in survey.items():
        col = coin_vectors.get(k)
        if col is None: continue
        coin_vectors['rank'] += col * v
    coin_vectors.index = coin_vectors['name']
    coin_vectors.fillna(0, inplace=True)
    sorti = coin_vectors.sort_values('rank')
    # this will only drop columns created by one hot
    drop_deez = [x for x in sorti.columns if '_' in x]
    drop_deez.extend(['risk', 'dev', 'social', 'usage', 'price', 'updown'])
    sorti.drop(drop_deez, axis=1, inplace=True)
    return sorti


def unpack_survey(survey):
    new_survey = {
        'risk':  survey.get('risk', 0),
        'updown': survey.get('updown', 0),
        'dev': survey.get('dev', 0),
        'social': survey.get('social', 0),
        'usage': survey.get('usage', 0),
        'price': survey.get('price', 0)
    }
    n = 5
    logger.debug('Survey received: %s' % (survey))

    for industry in survey.get('industries'):
        new_survey['industry_%s' % industry.title()] = n

    for mcap in survey.get('marketcaps'):
        new_survey['marketcap_%s' % mcap.title()] = n

    for age in survey.get('ages'):
        if age == 'pre-ico' or age == 'ico': continue
        new_survey['age_%s' % age.title()] = n

    logger.debug('Survey derived: %s' % (new_survey))
    # scale everything so it is a distribution, i.e. adds to 1
    sm = sum([float(x) for x in new_survey.values()])
    if sm == 0: sm = 1
    scaled = { k: float(v) / sm for k,v in new_survey.items() }
    return scaled