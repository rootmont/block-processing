import pandas as pd
from common.db import timeseries, connect
import datetime
from copy import deepcopy
import re
from dateutil.relativedelta import relativedelta


supported_frequencies = [
    'months',
    'years',
    'days',
    'weeks'
]



def get_filtered_prices(filter, freq):
    prices = timeseries.get_prices_frequency(freq)
    filtered = timeseries.filter_coins(filter, freq)

    # must shift prices bc we want to associate
    # the returns next period with the decision this period
    return prices.shift(-1), filtered


def filter_coins_today(filter):
    # take yesterday bc today's data is typically incomplete
    date = datetime.date.today() - datetime.timedelta(days=1)
    filtered = timeseries.filter_coins_date(date, filter)
    return filtered


def backtest(prices, filtered):
    if filtered is None or len(filtered) == 0: return {}
    chng = prices.pct_change().fillna(0).replace([pd.np.inf], 0)
    min_date = filtered['my_date'].min()
    max_date = filtered['my_date'].max()
    history = {}
    last_cumulative = 0
    for date in chng.loc[min_date:max_date].index:
        datestring = date.strftime('%Y/%m/%d')
        coins = filtered[filtered['my_date'] == date]['token_name'].unique()
        coins = sorted(coins)
        series = pd.Series(data=[chng.loc[date,x] for x in coins], index=coins)
        series.fillna(0, inplace=True)
        series['Average'] = series.mean()
        series.fillna(0, inplace=True)
        series['Cumulative'] = ((last_cumulative + 1) * (series['Average'] + 1)) - 1
        last_cumulative = series['Cumulative']
        history[datestring] = series.to_dict()
    return history


def initiate_backtest(filter, freq):
    prices, filtered = get_filtered_prices(filter, freq)
    results = backtest(prices, filtered)
    return results


def initiate_backtest_hodl(freq, periods, start_date=None, end_date=datetime.date.today()):
    if start_date is not None:
        date_min = start_date
        date_max = start_date + relativedelta(**{freq: periods + 1})
    else:
        date_min = end_date - relativedelta(**{freq: periods + 1})
        date_max = end_date
    hodl_filter = [
        { 'token_name': ['Bitcoin'] },
        { 'dates': [date_min, date_max] }
    ]
    return initiate_backtest(hodl_filter, freq)


def timelimit_filter(filter, frequency, periods, start_date=None, end_date=datetime.date.today()):
    new_filter = deepcopy(filter)
    if start_date is not None:
        date_min = start_date
        date_max = start_date + relativedelta(**{frequency: periods + 1})
    else:
        date_min = end_date - relativedelta(**{frequency: periods + 1})
        date_max = end_date
    new_filter.append({
        'dates': [date_min, date_max]
    })
    return new_filter

def initiate_backtest_all(filter, periods=10, start_date=None, end_date=datetime.date.today()):
    d = {}
    for frequency in supported_frequencies:
        new_filter = timelimit_filter(filter, frequency, periods, start_date=start_date, end_date=end_date)
        d[frequency] = initiate_backtest(new_filter, freq=frequency)
    d['hodl weeks'] = initiate_backtest_hodl('weeks', periods, start_date=start_date, end_date=end_date)
    d['hodl months'] = initiate_backtest_hodl('months', periods, start_date=start_date, end_date=end_date)
    d['hodl years'] = initiate_backtest_hodl('years', periods, start_date=start_date, end_date=end_date)
    return d


def get_filters_all():
    daily_stats_cols = connect.get_columns('daily_stats')
    regex = re.compile("_\d+")

    social_cols = {
        regex.split(x)[0]
        for x in daily_stats_cols
        if 'twitter_' in x
        or 'reddit' in x
        or 'bitcointalk_posts' in x
    }

    technical_cols = {
        regex.split(x)[0]
        for x in daily_stats_cols
        if 'alpha' in x
        or 'beta' in x
        or 'cov' in x
        or 'downside' in x
        or 'upside' in x
        or 'up_down' in x
        or 'sharpe' in x
        or 'sortino' in x
        or 'treynor' in x
        or 'r_squared' in x
    }

    usage_cols = {
        regex.split(x)[0]
        for x in daily_stats_cols
        if 'metcalfe' in x
        or 'transactions' in x
    }

    dev_cols = {
        regex.split(x)[0]
        for x in daily_stats_cols
        if 'github' in x
    }

    d = {
        "usage_ranking": {
            "name": "Usage Ranking",
            "filters": get_filters_for_cols(usage_cols, timed=False, all_cols=daily_stats_cols)
        },
        "social_ranking": {
            "name": "Social Ranking",
            "filters": get_filters_for_cols(social_cols, timed=False, all_cols=daily_stats_cols)
        },
        "development_ranking": {
            "name": "Development Metrics",
            "filters": get_filters_for_cols(dev_cols, timed=False, all_cols=daily_stats_cols)
        },
        "technical_analysis": {
            "name": "Technical Metrics",
            "filters": get_filters_for_cols(technical_cols, timed=True, all_cols=daily_stats_cols)
        },
        "core": {
            "name": "General",
            "filters": [
              {
                "name": "Price", 
                "dataKey": "price_close",
                "type": "compare",
                "options": {
                  "greater_than": True,
                  "less_than": True,
                  "percentile": False
                }
              },
              {
                "name": "Trade Volume", 
                "type": "compare",
                "dataKey": "trading_volume",
                "dataKey_percentile": "trading_volume_10000_rank_global",
                "options": {
                  "greater_than": True,
                  "less_than": True,
                  "percentile": True,
                }
              },
              {
                "name": "Circulating Supply", 
                "type": "compare",
                "dataKey": "supply",
                "options": {
                  "greater_than": True,
                  "less_than": True,
                  "percentile": False,
                }
              },
              {
                "name": "Root Rank",
                "type": "compare",
                "dataKey_percentile": "root_rank",
                "options": {
                  "greater_than": True,
                  "less_than": True,
                  "percentile": True,
                }
              },
              {
                "name": "Social Rank",
                "type": "compare",
                "dataKey_percentile": "social_rank",
                "options": {
                  "greater_than": True,
                  "less_than": True,
                  "percentile": True,
                }
              },
              {
                "name": "Usage Rank",
                "type": "compare",
                "dataKey_percentile": "usage_rank",
                "options": {
                  "greater_than": True,
                  "less_than": True,
                  "percentile": True,
                }
              },
              {
                "name": "Price Performance Rank",
                "dataKey_percentile": "risk_rank",
                "options": {
                  "greater_than": True,
                  "less_than": True,
                  "percentile": True,
                }
              },
              {
                "name": "Development Rank",
                "type": "compare",
                "dataKey_percentile": "dev_rank",
                "options": {
                  "greater_than": True,
                  "less_than": True,
                  "percentile": True,
                }
              },
              {
                "name": "Age", 
                "type": "age_cluster",
              },
              {
                "name": "Market Cap", 
                "type": "marketcap_cluster",
              },
              {
                "name": "Industry", 
                "type": "industry_cluster"
              }
            ]
        }
    }
    return d


def has_rank(tgt, cols):
    for x in cols:
        f = re.findall('%s.*_rank_global.*' % tgt, x)
        if len(f) > 0:
            return f[0]
    return None


def get_filters_for_cols(cols, timed, all_cols):
    filters = []
    timed_str = '_%days%' if timed else ''
    for x in cols:
        if 'rank' in x: continue
        f = {
            "name": x.title().replace('_',' '),
            "type": "compare",
            "dataKey": x + timed_str,
            "timed_input": timed
        }
        rank = has_rank(x, all_cols)
        if rank is not None:
            f["dataKey_percentile"] = re.sub(r'\d*', '', rank).replace('__','_%days%_')
        f["options"] = {
            "greater_than": True,
            "less_than": True,
            "percentile": rank is not None
        }
        filters.append(f)
    return filters

