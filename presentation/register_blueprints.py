import logging
import os
import sys

sys.path.append(os.path.abspath(os.sep.join([".."])))

from app import application
from api.coin_api import blueprint as coin_api_blueprint
from api.backtest_api import blueprint as backtest_api_blueprint
from api.dashboard_api import blueprint as dashboard_api_blueprint
from api.catalog_api import blueprint as catalog_api_blueprint
from api.vector_api import blueprint as vector_api_blueprint
from api.benchmarks_api import blueprint as benchmarks_api_blueprint
from api.market_api import blueprint as market_api_blueprint

from common.env import DBHOST, DBUSER, DBDATABASE, DBPORT
from common.config import format_logger

logger = logging.getLogger('presentation')
format_logger(logger)


def has_no_empty_params(rule):
    defaults = rule.defaults if rule.defaults is not None else ()
    arguments = rule.arguments if rule.arguments is not None else ()
    return len(defaults) >= len(arguments)


if __name__ == '__main__':
    # Register all the blueprints
    application.flask_app.register_blueprint(coin_api_blueprint)
    application.flask_app.register_blueprint(backtest_api_blueprint)
    application.flask_app.register_blueprint(dashboard_api_blueprint)
    application.flask_app.register_blueprint(vector_api_blueprint)
    application.flask_app.register_blueprint(catalog_api_blueprint)
    application.flask_app.register_blueprint(benchmarks_api_blueprint)
    application.flask_app.register_blueprint(market_api_blueprint)

    logger.info(application.flask_app.url_map)
    logger.info('Using db info: %s, %s, %s, %s' % (DBHOST, DBUSER, DBDATABASE, DBPORT))
    # logging.info([str(p) for p in application.flask_app.url_map.iter_rules()])
    # Start the application
    application.run()
