from common.db import coin, risk, daily_stats
from common.rank_tree import rank_tree
import logging

timespans = {
    '1 week': 7,
    '1 month': 30,
    '3 months': 90,
    '6 months': 180,
    '1 year': 360,
    '2 years': 720
}


def get_coin_report(symbol):
    name = coin.get_name_for_symbol(symbol)
    all_stats = daily_stats._get_row(name)
    if all_stats is None: return {}
    clusters = coin.get_clusters(name)
    age_cluster = clusters['age_cluster']
    mcap_cluster = clusters['marketcap_cluster']
    industry_cluster = coin.get_category_for_coin(name)
    urls = coin.get_urls(name)

    portfolio = {}
    up_down = {}
    for time, num_days in timespans.items():
        portfolio[time] = {}
        up_down[time] = {}
        for category in ['industry', 'age', 'marketcap', 'global']:
            portfolio[time][category] = {}
            up_down[time][category] = {}
            if category == 'marketcap':
                cat = 'mcap'
            else:
                cat = category
            for col in risk.risk_columns:
                if col == 'price' or col == 'marketcap' or col == 'cov' or col == 'r_squared': continue
                if col != 'upside' and col != 'downside':
                    if col == 'up_down':
                        pretty_col = 'up/Down'
                    elif col == 'r_squared':
                        pretty_col = 'r-Squared'
                    else:
                        pretty_col = col
                    portfolio[time][category][pretty_col] = {
                        'score': all_stats['%s_%s_%s' % (col, num_days, cat)],
                        'percentile': all_stats['%s_%s_rank_%s' % (col, num_days, cat)]
                    }
                else:
                    up_down[time][category][col] = {
                        'score': all_stats['%s_%s_%s' % (col, num_days, cat)],
                        'percentile': all_stats['%s_%s_rank_%s' % (col, num_days, cat)]
                    }
    d = {
        'portfolio': portfolio,
        'up/down': up_down,
        'website_url': urls['website_url'],
        'whitepaper_url': urls['whitepaper_url'],
        'description': coin.get_description(name),
        'disclosure': coin.get_disclosure(name),
        'categories': {
            'age': age_cluster,
            'marketcap': mcap_cluster,
            'industry': industry_cluster
        },
        'percentiles': {
            'social': all_stats['social_rank'],
            'usage': all_stats['usage_rank'],
            'price performance': all_stats['risk_rank'],
            'development': all_stats['dev_rank'],
            'overall': all_stats['root_rank']
        },
        'social': {
            key: {
                'score': all_stats[value],
                'percentile': all_stats['{}_rank_global'.format(value)]
            }
            for key, value in rank_tree['root_rank']['social_rank'].items()
        },
        'development': {
            key: {
                'score': all_stats[value],
                'percentile': all_stats['{}_rank_global'.format(value)]
            }
            for key, value in rank_tree['root_rank']['dev_rank'].items()
        },
        'usage': {
            key: {
                'score': all_stats[value],
                'percentile': all_stats['{}_rank_global'.format(value)]
            }
            for key, value in rank_tree['root_rank']['usage_rank'].items()
        },
        'price performance': {
            key: {
                'score': all_stats[value],
                'percentile': all_stats['{}_rank_global'.format(value.split('_global')[0] )]
            }
            for key, value in rank_tree['root_rank']['risk_rank'].items()
        }
    }

    # add percentiles
    d['social']['percentile'] = all_stats['social_rank']
    d['development']['percentile'] = all_stats['dev_rank']
    d['usage']['percentile'] = all_stats['usage_rank']
    d['price performance']['percentile'] = all_stats['risk_rank']

    return d



