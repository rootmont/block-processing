from common.db import coin, timeseries, risk, daily_stats
import pandas as pd
import datetime


######### CATALOG #############

def get_catalog(age_cluster=None, mcap_cluster=None, industry_cluster=None):
    catalog = daily_stats._get_catalog(age_cluster=age_cluster, mcap_cluster=mcap_cluster, industry_cluster=industry_cluster)
    return sorted(catalog, key=lambda x: x['overall percentile'])
