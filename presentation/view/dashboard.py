import pandas as pd
import datetime
import logging
from common.db import timeseries, risk, daily_stats

logger = logging.getLogger('api')
# data frame
master = None
last_update = None
mutated_columns = ["alpha", "beta", "market_cap", "marketcap", "sharpe"]


def filter_master_table(rows, cols):
    master = get_master_table()
    cols = [col.replace("market_cap", "marketcap") for col in cols]
    cols = [col if col != 'daily_transactions' else 'daily_transactions_10000' for col in cols]
    cols = [col if col not in risk.risk_columns else col + "_90_global" for col in cols]
    filtered_cols = [col for col in cols if col in master.columns]
    if filtered_cols != cols:
        logger.warning('Unusable column(s) in request: %s' % [x for x in cols if x not in filtered_cols])
    filtered_master = master.loc[rows,filtered_cols]
    filtered_master.rename(columns=lambda col: col if '_90_global' not in col else col.replace('_90_global', ''), inplace=True)
    filtered_master.rename(columns=lambda col: col.replace("marketcap", "market_cap"), inplace=True)
    filtered_master.rename(columns=lambda col: col.replace("_10000", ""), inplace=True)
    filtered_master.dropna(axis=0, inplace=True)
    if filtered_master.duplicated().any():
        dupes = filtered_master[filtered_master.duplicated()].index
        logger.warning('Duplicates in index, dropping {}'.format(dupes))
    filtered_master.drop_duplicates(inplace=True)
    return filtered_master.to_dict('index')


def get_master_table():
    global master
    global last_update
    now = datetime.datetime.now()
    # update every 12 hours
    if master is None or (now - last_update).seconds > 43200:
        stats = daily_stats.get_all()
        master = pd.DataFrame(stats, index=[x['token_name'] for x in stats])
        master.drop(['token_name', 'coin_id', 'updated'], axis=1, inplace=True)
        master.sort_index(axis=0, inplace=True)
        last_update = datetime.datetime.now()

    return master


def get_master_table_columns():
    master = get_master_table()
    return master.columns.tolist()


def get_master_table_rows():
    master = get_master_table()
    return master.index.tolist()


def get_movers(days_back=7, num_movers=5):
    movers = {}
    yesterday = datetime.date.today() - datetime.timedelta(days=2)
    price_df = timeseries.get_recent_transactions(end_date=yesterday, days_back=days_back + 2).fillna(0)
    price_df = price_df[price_df['price_close'] > 0]
    price_df = price_df.drop_duplicates().pivot("my_date", "token_name").fillna(0)
    pct_change = price_df.pct_change(periods=days_back)
    pct_change = pct_change.fillna(0).replace([pd.np.inf, -pd.np.inf], 0)
    vol_df = price_df.pct_change(axis=0).tail(days_back).std().fillna(0)
    # vol_df = price_df.tail(days_back).std()

    movers['price_losers'] = pct_change['price_close'].iloc[-1].sort_values().iloc[:num_movers].to_dict()
    movers['price_winners'] = pct_change['price_close'].iloc[-1].sort_values().iloc[-num_movers:].to_dict()
    movers['price_active'] = vol_df['price_close'].sort_values().iloc[-num_movers:].to_dict()
    movers['ntx_losers'] = pct_change['daily_transactions'].iloc[-1].sort_values().iloc[:num_movers].to_dict()
    movers['ntx_winners'] = pct_change['daily_transactions'].iloc[-1].sort_values().iloc[-num_movers:].to_dict()
    movers['ntx_active'] = vol_df['daily_transactions'].sort_values().iloc[-num_movers:].to_dict()

    return movers
