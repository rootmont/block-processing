from common.db import benchmarks
import logging

logger = logging.getLogger('presentation')

cluster_types=['industry', 'age', 'mcap']


def get_quantiles(cluster_type):
    if cluster_type not in cluster_types:
        msg = 'Invalid cluster_type %s' % cluster_type
        logger.error(msg)
        raise ValueError(msg)

    local = benchmarks.get_latest_benchmarks_for_type(cluster_type=cluster_type)
    global_ = benchmarks.get_latest_benchmarks_for_type(cluster_type='global')
    if local is None and global_ is None:
        logger.error('Unable to get benchmark data')
        return []
    data = local + global_
    d = {}
    for item in data:
        key = item['metric']
        cat = item['cluster_name']
        obj = {
            cat: {
                'mean': item['mean'],
                'std': item['std'],
                'min': item['min'],
                '25%': item['25%'],
                '50%': item['50%'],
                '75%': item['75%'],
                'max': item['max'],
                'count': item['count']
            }
        }
        if key in d:
            d[key].append(obj)
        else:
            d[key] = [obj]

    dd = [{k: v} for k, v in sorted(d.items(), key=lambda x: x[0])]

    return dd
