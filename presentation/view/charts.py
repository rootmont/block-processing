from common.db import coin, timeseries, daily_stats
import pandas as pd
import datetime


# TODO: cache this daily?
def get_metcalfe_stats(name):
    df = timeseries.get_ntx_price_for_name(token_name=name)
    met = daily_stats.get_metcalfe_for_name(token_name=name)
    if met is None or None in met.values():
        metcalfe_exponent = 0
        # normalized_metcalfe = 0
        metcalfe_proportion = 0
    else:
        metcalfe_exponent = met['metcalfe_exponent_10000']
        # normalized_metcalfe = met['normalized_metcalfe_10000']
        metcalfe_proportion = met['metcalfe_proportion_10000']

    df.rename(columns={'daily_transactions': 'ntx', 'price_close':'price', 'my_date':'date'}, inplace=True)

    df['raw_metcalfe'] = df["ntx"] ** metcalfe_exponent
    df['raw_metcalfe'].replace(pd.np.inf, 0, inplace=True)

    # metcalfe_proportion = normalized_metcalfe / raw_metcalfe
    # metcalfe_proportion = normalized_metcalfe / df['raw_metcalfe'].iloc[-1]
    df['normalized_metcalfe'] = df['raw_metcalfe'] * metcalfe_proportion
    df['normalized_metcalfe'].replace(pd.np.inf, 0, inplace=True)

    df['ntx_divergence'] = df['normalized_metcalfe'] - df["price"]

    df['metcalfe_price_ratio'] = df['normalized_metcalfe'] / df["price"]

    df['metcalfe_movement'] = df['normalized_metcalfe'].pct_change()

    return df


def chart_format(ts):
    ts.replace([pd.np.inf, -pd.np.inf], pd.np.nan, inplace=True)
    ts.fillna(0, inplace=True)
#    ts.replace(pd.np.nan, , inplace=True)
    l = []
    for i in range(len(ts)):
        d = {
            'date': ts.index[i].strftime('%Y-%m-%d'),
            'score': float(ts.iloc[i])
        }
        l.append(d)
    return l


def get_metcalfe_chart_data(symbol):
    name = coin.get_name_for_symbol(symbol)
    usage = get_metcalfe_stats(name)
    usage['date'] = usage.index
    # put NaN if we don't have the ntx data
    if all(usage['ntx'] == 0):
        usage['ntx'] = pd.np.nan
        usage['raw_metcalfe'] = pd.np.nan
        usage['metcalfe_price_ratio'] = pd.np.nan
        usage['metcalfe_movement'] = pd.np.nan
        usage['normalized_metcalfe'] = pd.np.nan
        usage['ntx_divergence'] = pd.np.nan
    else:
        usage.fillna(0, inplace=True)
        usage.replace(pd.np.inf, 0, inplace=True)
    return usage


def get_price_chart_data(symbol):
    df = timeseries.get_price_chart_data(symbol)
    df.rename({'my_date':'date'}, axis=1, inplace=True)
    prices = df["price"]
    raw, avg, div = get_macd_df(prices)
    df = df.assign(price_first_derivative=raw,price_smooth_derivative=avg,divergence=div)
    # clip the last one bc sometimes it's zero
    df = df[df['price'] > 0]
    return df



def get_macd_df(ts, fast=12, slow=26, average=9, ratio=1/2):
    if type(ts) == pd.core.series.Series: ts = pd.DataFrame(ts)
    fast_ema = exp_moving_avg(ts, fast, ratio)
    slow_ema = exp_moving_avg(ts, slow, ratio)
    macd = fast_ema - slow_ema
    avg = exp_moving_avg(macd, average, ratio)
    div = avg - macd
    return macd, avg, div


def exp_moving_avg(data, window, ratio):
    ema = pd.DataFrame(columns=data.columns, index=data.index).fillna(0)
    # do you copy? roger that
    x = pd.DataFrame(data) * ratio
    for i in range(window):
        ema += x
        x = x.shift().fillna(0) * ratio
    return ema

