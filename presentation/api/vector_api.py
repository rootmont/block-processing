import json

from flask_cors import cross_origin
from flask import request, abort, render_template, Blueprint
from presentation.controller.vectors import get_coins_for_survey
from app import application
import logging
logger = logging.getLogger('presentation')

blueprint = Blueprint('vector_api', __name__, template_folder='./templates')


@blueprint.route("/suggestions/", methods=['POST'])
@cross_origin()
def get_suggestions():
    try:
        email = request.json.get("email", '')
        if len(email) == 0: abort(400)
        coin_list = get_coins_for_survey(request.json).to_dict('index')
        logger.debug('Sending {} to {}'.format(coin_list, email))
        create_email(email, coin_list)
        return json.dumps({'message': 'Success'})
    except json.JSONDecodeError:
        abort(400)


def send_email(email, body):
    # send single recipient; single email as string
    try:
        # application.mail._personalizations = []
        application.mail.send_email(
            from_email='info@rootmont.com',
            to_email=email,
            subject='Your personalized coin list has arrived!',
            html=body
        )
    except Exception as e:
        print(e)


def create_email(email, coin_list):
    body = render_template("flask_email_template.html", coin_list=coin_list)
    send_email(email, body)
