import json
from flask_cors import cross_origin
from flask import Blueprint

from common.db import coin, current_stats
from view import catalog
from app import application

blueprint = Blueprint('reports_api', __name__, template_folder='./templates')

# one hour
cache_seconds = 60 * 60


@blueprint.route("/catalog/global")
@cross_origin()
@application.cache.memoize(timeout=cache_seconds)
def get_catalog_global():
    return json.dumps(catalog.get_catalog())


@blueprint.route("/catalog/industry/<industry>")
@blueprint.route("/catalog/industries/<industry>")
@cross_origin()
@application.cache.memoize(timeout=cache_seconds)
def get_catalog_industry(industry):
    return json.dumps(catalog.get_catalog(industry_cluster=industry))


@blueprint.route("/catalog/marketcap/<marketcap>")
@blueprint.route("/catalog/marketcaps/<marketcap>")
@cross_origin()
@application.cache.memoize(timeout=cache_seconds)
def get_catalog_marketcap(marketcap):
    return json.dumps(catalog.get_catalog(mcap_cluster=marketcap))


@blueprint.route("/catalog/age/<age>")
@blueprint.route("/catalog/ages/<age>")
@cross_origin()
@application.cache.memoize(timeout=cache_seconds)
def get_catalog_age(age):
    return json.dumps(catalog.get_catalog(age_cluster=age))


@blueprint.route("/all-categories")
@cross_origin()
@application.cache.memoize(timeout=cache_seconds)
def get_all_categories():
    d = {
        'industries': [x['category'] for x in coin.get_all_categories()],
        'ages': [x['age_cluster'] for x in coin.get_all_age_clusters()],
        'marketcaps': [x['marketcap_cluster'] for x in coin.get_all_marketcap_clusters()]
    }
    return json.dumps(d)


@blueprint.route("/all-coins")
@cross_origin()
@application.cache.memoize(timeout=cache_seconds)
def get_all_coins():
    return json.dumps(coin.get_all_coins())


@blueprint.route("/all-coin-names")
@cross_origin()
@application.cache.memoize(timeout=cache_seconds)
def get_all_coin_names():
    return json.dumps(coin.get_all_coin_names())


@blueprint.route("/prices")
@cross_origin()
def get_all_price_data():
    ps = current_stats.get_all_prices()
    return json.dumps(ps)
