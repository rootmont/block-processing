import json
import logging
from flask import request, abort, Blueprint
from flask_cors import cross_origin

from view import dashboard
from common.db import timeseries
from app import application

blueprint = Blueprint('dashboard_api', __name__, template_folder='./templates')
cache_seconds = 60 * 60 * 12 - 1

logger = logging.getLogger('api')

@blueprint.route("/health")
@cross_origin()
def health():
    return "OK", 200


@blueprint.route("/master-table/")
@cross_origin()
def get_master_table():
    rows = request.args.get('rows')
    cols = request.args.get('cols')
    try:
        rows = json.loads(rows)
        cols = json.loads(cols)
    except (json.JSONDecodeError, TypeError):
        logger.error('/master-table/ got bad json %s %s' % (rows, cols))
        abort(400)
    return json.dumps(dashboard.filter_master_table(rows, cols))


@blueprint.route("/master-table/all")
@cross_origin()
def get_master_table_all():
    return json.dumps(dashboard.get_master_table().to_dict('index'))


@blueprint.route("/master-table/columns")
@cross_origin()
def get_master_table_columns():
    return json.dumps(dashboard.get_master_table_columns())


@blueprint.route("/master-table/rows")
@cross_origin()
def get_master_table_rows():
    return json.dumps(dashboard.get_master_table_rows())


@blueprint.route("/ntx/")
@cross_origin()
def get_ntx():
    symbols = request.args.get('symbols', []).split(',')
    return timeseries.get_all_ntxs(symbols).to_json(date_format='iso')


@blueprint.route("/vol/")
@cross_origin()
def get_vol():
    symbols = request.args.get('symbols', []).split(',')
    return timeseries.get_all_volumes(symbols).to_json(date_format='iso')


@blueprint.route("/mcap/")
@cross_origin()
def get_mcap():
    symbols = request.args.get('symbols', []).split(',')
    return timeseries.get_all_marketcaps(symbols).to_json(date_format='iso')


@blueprint.route("/price/")
@cross_origin()
def get_price():
    symbols = request.args.get('symbols', []).split(',')
    return timeseries.get_all_price_close(symbols).to_json(date_format='iso')


@blueprint.route("/price/open/")
@cross_origin()
def get_price_open():
    symbols = request.args.get('symbols', []).split(',')
    return timeseries.get_all_price_open(symbols).to_json(date_format='iso')


@blueprint.route("/supply/")
@cross_origin()
def get_supply():
    symbols = request.args.get('symbols', []).split(',')
    return timeseries.get_all_supplies(symbols).to_json(date_format='iso')


@blueprint.route("/movers/<int:days_back>/<int:num_movers>")
@cross_origin()
@application.cache.memoize(timeout=cache_seconds)
def get_movers(days_back, num_movers):
    return json.dumps(dashboard.get_movers(days_back=days_back, num_movers=num_movers))

