from flask_cors import cross_origin
from flask import redirect, Blueprint
from common.db import timeseries, ranks
import datetime
import json
import pandas as pd
from common.db import coin, daily_stats, current_stats

from app import application
from view import charts, coin_report

blueprint = Blueprint('market_api', __name__, template_folder='./templates')
cache_seconds = 60 * 60 * 12 - 1


def get_weekly_update_movers(start_date: datetime.date, days_back: int):
    df = timeseries.get_weekly_update_movers(start_date)
    end_date = start_date + datetime.timedelta(days=7)
    # df.sort_values(by='token_name', inplace=True)
    mcaps = pd.DataFrame(current_stats.get_mcaps_today(), columns=['marketcap', 'token_name'])
    recs = df.to_dict(orient='records')
    for rec in recs:
        if rec['my_date'] == end_date and rec['token_name'] in mcaps['token_name'].values:
            rec['marketcap'] = mcaps[mcaps['token_name'] == rec['token_name']]['marketcap'].values[0]
    df = pd.DataFrame(recs)
    # mcaps.sort_values(by='token_name', inplace=True)
    # intersection = set(mcaps.token_name.values).intersection(df.token_name.values)
    # df = df.loc[df.token_name.isin(intersection)]
    # mcaps = mcaps.loc[mcaps.token_name.isin(intersection)]
    df.drop_duplicates(subset=['token_name', 'my_date'], inplace=True)
    piv = df.pivot(columns='token_name', index='my_date')
    chng = piv.pct_change(periods=7).iloc[-1 - days_back]
    ret = chng.unstack()
    ret.index = pd.Index(['marketcap % change', 'price % change', 'volume % change' ])
    # ret.loc['marketcap'] = mcaps.T.loc['marketcap']
    return ret, mcaps


def get_global_marketcap_movements(start_date: datetime.date):
    df = timeseries.get_global_marketcaps(start_date)
    df['weekly_change'] = df['global_marketcap'].astype(float).pct_change(periods=7)
    return df

def get_rank_changes(start_date, days_back: int):
    df = ranks.get_ranks_since_date(start_date)
    piv = df.pivot(index='my_date', columns='token_name')
    chng = piv.pct_change(periods=7).iloc[-1 - days_back]
    return chng.unstack()


@blueprint.route('/market-minute/<datestring>')
@cross_origin()
def get_market_minute(datestring):
    start_date = datetime.datetime.strptime(datestring, '%Y-%m-%d')
    days_back = 0
    df1 = get_weekly_update_movers(start_date, days_back)
    df2 = get_global_marketcap_movements(start_date)
    df3 = get_rank_changes(start_date, days_back)
    return json.dumps({
        'weekly_update_movers': df1[0].to_csv(),
        'marketcaps': df1[1].to_csv(),
        'global_mcap': df2.to_csv(),
        'rank_movers': df3.to_csv(),

    })

