from flask_cors import cross_origin
from flask import request, Blueprint, abort
import json
from controller.backtest import supported_frequencies, initiate_backtest, \
    initiate_backtest_all, get_filters_all, timelimit_filter
import logging
from common.db import timeseries
from common.config import format_logger


blueprint = Blueprint('backtest_api', __name__, template_folder='./templates')
logger = logging.getLogger('backtest api')
format_logger(logger)


@blueprint.route("/backtest/frequencies")
@blueprint.route("/backtest/strategies/")
@cross_origin()
def get_strategies():
    return json.dumps(supported_frequencies)


@blueprint.route("/backtest/<strategy>/", methods=['POST'])
@cross_origin()
def backtest_with_strategy(strategy):
    filter = request.json
    if strategy not in supported_frequencies: abort(401)
    results = initiate_backtest(filter, freq=strategy)
    return json.dumps(results)


@blueprint.route("/backtest/", methods=['POST'])
@cross_origin()
def backtest():
    filter = request.json
    d = initiate_backtest_all(filter)
    return json.dumps(d)


@blueprint.route("/backtest/filter/", methods=['POST'])
@cross_origin()
def filter_coins():
    filter = request.json
    logger.info(filter)
    # date = datetime.date.today() - datetime.timedelta(days=1)
    new_filter = timelimit_filter(filter=filter, frequency='days', periods=130)
    logger.info(new_filter)
    # results = backtest.filter_coins_today(filter)
    results = timeseries.filter_coins(new_filter, 'weeks')
    logger.debug(results.to_json(date_format='iso', orient='records'))
    return results.to_json(date_format='iso', orient='records')


@blueprint.route("/backtest/filter-list/", methods=['GET'])
@cross_origin()
def filter_list():
    return json.dumps(get_filters_all())
