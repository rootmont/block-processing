from flask_cors import cross_origin
from flask import redirect, Blueprint
from datetime import timezone
import json

from common.db import coin, daily_stats, current_stats

from app import application
from view import charts, coin_report

blueprint = Blueprint('coin_api', __name__, template_folder='./templates')
cache_seconds = 60 * 60 * 12 - 1


@blueprint.route('/daily-stats')
@cross_origin()
def get_daily_stats():
    all_daily = daily_stats.get_all()
    for s in all_daily:
        s["updated"] = int(s["updated"].replace(tzinfo=timezone.utc).timestamp())
        del s["coin_id"]
        del s["id"]
    return json.dumps(all_daily)


@blueprint.route('/metcalfe-charts/<symbol>')
@cross_origin()
@application.cache.memoize(timeout=cache_seconds)
def get_metcalfe_chart_data(symbol):
    df = charts.get_metcalfe_chart_data(symbol)
    return df.to_json(date_format='iso', orient='records')


@blueprint.route('/price-charts/<symbol>')
@cross_origin()
@application.cache.memoize(timeout=cache_seconds)
def get_price_chart_data(symbol):
    df = charts.get_price_chart_data(symbol)
    return df.to_json(date_format='iso', orient='records')


@blueprint.route("/coin-report/<symbol>")
@cross_origin()
@application.cache.memoize(timeout=cache_seconds)
def get_coin_report(symbol):
    return json.dumps(coin_report.get_coin_report(symbol))


@blueprint.route("/all-disclosure")
@cross_origin()
@application.cache.memoize(timeout=cache_seconds)
def get_all_disclosure():
    return json.dumps(coin.get_disclosure_all())


@blueprint.route("/")
def hello():
    return redirect('https://rootmont.com')


@blueprint.route("/current-stats/<name>")
def get_current_stats(name):
    return json.dumps(current_stats.get_one(name))


# if __name__ == "__main__":
#    app.run(host='0.0.0.0')

    # resource = WSGIResource(reactor, reactor.getThreadPool(), app)
    # site = Site(resource)
    # reactor.listenTCP(5555, site)
    # reactor.run()


