import json

from flask_cors import cross_origin
from flask import Blueprint

from app import application
from view import benchmark_report


blueprint = Blueprint('benchmarks_api', __name__, template_folder='./templates')
cache_seconds = 60 * 60 * 12 - 1


@blueprint.route("/benchmark/marketcap")
@cross_origin()
@application.cache.memoize(timeout=cache_seconds)
def get_benchmark_marketcap():
    return json.dumps(benchmark_report.get_quantiles('mcap'))


@blueprint.route("/benchmark/age")
@cross_origin()
@application.cache.memoize(timeout=cache_seconds)
def get_benchmark_age():
    return json.dumps(benchmark_report.get_quantiles('age'))


@blueprint.route("/benchmark/industry")
@cross_origin()
@application.cache.memoize(timeout=cache_seconds)
def get_benchmark_industry():
    return json.dumps(benchmark_report.get_quantiles('industry'))
