block-processing
---
Suite of servers designed to pull information regarding blocks on chains and process the information.

### Requirements
* docker
* docker-compose >= 1.23

Please download and install the latest `docker-compose` from the github repo: [https://github.com/docker/compose/releases](https://github.com/docker/compose/releases). We need the force recreate feature to remove the database. The version distributed by Ubuntu is way behind.

Check your `docker-compose` version with `docker-compose --version`.

#### Steps for manual `docker-compose` installation:
1) Download the corresponding docker-compose version for your operating system.
2) `chmod 755 ./downloaded-docker-compose`
3) `chown root:root ./downloaded-docker-compose`
4) `which docker-compose` and note the result.
5) `sudo rm $(which docker-compose)`
6) `sudo mv ./downloaded-docker-compose /location/from/step/4`
7) check version again with `docker-compose --version`

### Running

Standard build:
```
./run.sh
```

Rebuilding from scratch:
```
./run.sh -r
```

### Components

#### gathering

Gathers information from relevant sources (forums, blockchains, etc) and places them into the database layer.

#### presentation

HTTP API for information stored in the database.

#### common

### Copyright

All rights reserved, Rootmont 2019.
