import datetime
import os
from web3 import HTTPProvider, Web3
from common.env import INFURAPROJECTID, DEVENV, CMC_API_KEY

# import subprocess
# logger.debug('Current git commit: {}'.format(subprocess.check_output(["git", "describe", "--always"]).strip()))


os.environ['TZ'] = 'UTC'

# firefox ?
firefox_headers = {
    "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
    "cache-control": "max-age=0",
    "scheme": "https",
    "cookie": "__cfduid=d420f92bda370ea83f73a630429ee9d4f1524988388; _xicah=c9961c3a-fca776fa",
    "accept-language": "en-US",
    "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36",
    "method": "GET",
    "accept-encoding": "",
    "upgrade-insecure-requests": "1",
}

# proxyd = {"https": "https://107.170.113.28:31380", "http": "http://107.170.113.28:31380"}
# request_args = {'headers': firefox_headers, 'proxies': proxyd}
request_args = {'headers': firefox_headers}
gsheet_url = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vTGh4W0Nfq3v-AxXVXDoCNjrIx9Y8B5DO_nCjJaLlSTYhdV97fZYHfNu2JAPfl0-R_yMH5A3DQk0fn0/pub?gid=0&single=true&output=tsv'
transfer_topic = '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef'

infura_url = 'https://mainnet.infura.io/v3/%s' % INFURAPROJECTID
provider = HTTPProvider(infura_url)
web3 = Web3(provider)
nomics_date_format = '%Y-%m-%d'
all_time_days_back = 10000
backtest_days_back = 30
backfill_all_timeseries = False
earliest_crypto_date = datetime.date(2013, 1, 1)
earliest_backfill_date = datetime.date(2018, 11, 1) if DEVENV else earliest_crypto_date
earliest_ethereum_date = datetime.date(2015, 8, 8)
earliest_eos_date = datetime.date(2018, 6, 8)
earliest_nomics_supply_date = datetime.date(2017, 12, 15)
delete_absent_coins = True
parallel = False
cmc_headers = {
    'X-CMC_PRO_API_KEY': CMC_API_KEY
}
coin_logo_directory = os.getenv('ROOTMONTPATH', '../') + '../data/images'
age_clusters = ['Emerging', 'Established', 'Mature']
mcap_clusters = ['Micro Cap', 'Small Cap', 'Medium Cap', 'Large Cap']
industry_clusters = ['Platform', 'Gaming', 'Computing', 'Exchange', 'Payments', 'Blockchain Interoperability', 'Asset Backed', 'Enterprise', 'Media', 'Public Services', 'IOT', 'Social', 'Finance', 'Privacy', 'Logistics']
update_recipients = ['autumn@rootmont.com', 'chris@rootmont.com']
seconds_per_day = 60 * 60 * 24
seconds_per_eth_block = 17
blocks_per_day = seconds_per_day // seconds_per_eth_block
sliding_window_sizes = [7,30,90,180, 360, 720, 10000]
# sliding_window_sizes = [10000]
