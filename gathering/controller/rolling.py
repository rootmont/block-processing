import pandas as pd
from scipy.stats import linregress


# apply func to a rolling window of length "window" to the input and store it in the output
def rolling_window(func, input, output, window):
    # iterate over right hand of window
    for end in range(window, len(input)):
        # apply func to input[end-window:end] and store as row in output
        output.iloc[end] = func(input.head(end).tail(window))
    # chop off the first window rows, as it will be undefined
    return output.tail(-window)


def get_updown(individuals, benchmark, window):
    # both = pd.concat([individuals, benchmark], axis=1)
    # pct_mvmt = both.pct_change().fillna(0)
    # pct_mvmt.replace([pd.np.inf, -pd.np.inf], 0, inplace=True)
    individual_mvmt_pct = individuals.pct_change().fillna(0)
    individual_mvmt_pct.replace([pd.np.inf, -pd.np.inf], 0, inplace=True)
    benchmark_mvmt_pct = benchmark.pct_change().fillna(0)
    benchmark_mvmt_pct.replace([pd.np.inf, -pd.np.inf], 0, inplace=True)
    both = pd.concat([individual_mvmt_pct, benchmark_mvmt_pct], axis=1)

    upside = pd.DataFrame(columns=individual_mvmt_pct.columns, index=individual_mvmt_pct.index)
    downside = pd.DataFrame(columns=individual_mvmt_pct.columns, index=individual_mvmt_pct.index)

    upside = rolling_window(func=up, input=both, output=upside, window=window)
    downside = rolling_window(func=down, input=both, output=downside, window=window)

    return upside, downside


def up(df):
    benchmark = df['benchmark']
    individuals = df.drop(columns=['benchmark'], inplace=False)
    ind_up = individuals[individuals > 0].sum()
    ben_up = benchmark[benchmark > 0].sum()
    return ind_up.div(ben_up, axis=0) * 100


def down(df):
    benchmark = df['benchmark']
    individuals = df.drop(columns=['benchmark'], inplace=False)
    ind_down = individuals[individuals < 0].sum()
    ben_down = benchmark[benchmark < 0].sum()
    return ind_down.div(ben_down, axis=0) * 100


def get_metcalfe(df, window):
    levels = [
        list(df.columns.levels[1]),
        ['metcalfe_correlation', 'metcalfe_exponent', 'metcalfe_proportion']
    ]
    # multi = pd.MultiIndex(levels=levels, names=['name','metric'])
    multi = pd.MultiIndex.from_product(levels, names=['name', 'metric'])
    # multi = pd.MultiIndex.from_arrays(levels, names=['name','metric'])
    output = pd.DataFrame(index=df.index, columns=multi)
    met = rolling_window(func=metcalfe, input=df, output=output, window=window)
    return met


def metcalfe(df):
    ntx = df["daily_transactions"]
    prices = df["price_close"]

    log_prices = pd.np.log(prices[prices > 0])
    log_ntxs = pd.np.log(ntx[ntx > 0])

    exps = pd.Series(index=log_prices.columns, name='metcalfe_exponent')
    props = pd.Series(index=log_prices.columns, name='metcalfe_proportion')
    raw_met = pd.DataFrame(columns=log_prices.columns, index=log_prices.index)
    for i, col in enumerate(log_prices.columns):
        rel_log_ntx = log_ntxs[col][(log_ntxs[col].notnull()) & (log_prices[col].notnull())]
        rel_log_price = log_prices[col][(log_ntxs[col].notnull()) & (log_prices[col].notnull())]
        if len(rel_log_ntx) == 0 or len(rel_log_price) == 0:
            raw_met[col] = 0
            exps[col] = 0
            props[col] = 0
        else:
            metcalfe_exponent, metcalfe_proportion, _, _, _ = linregress(rel_log_ntx, rel_log_price)
            exps[col] = metcalfe_exponent
            props[col] = pd.np.e ** metcalfe_proportion
            raw_met[col] = ntx[col] ** metcalfe_exponent

    # may be inf if ntx is zero
    raw_met.replace(pd.np.inf, 0, inplace=True)

    met_corr = pd.Series(index=raw_met.columns, name='metcalfe_correlation')
    for col in raw_met.columns:
        rel_prices = prices[col][(prices[col] > 0) & (raw_met[col].notnull())]
        rel_met = raw_met[col][(prices[col] > 0) & (raw_met[col].notnull())]
        met_corr[col] = rel_prices.corr(rel_met)

    met_corr.fillna(0, inplace=True)
    df = pd.concat([met_corr, exps, props], axis=1)
    stacked = df.stack()
    stacked.index.names =['name', 'metric']
    return stacked
