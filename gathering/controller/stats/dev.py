import datetime
import logging
from common.db import coin
from common.db.github_db import GithubData
from sensors import github
from common.config import format_logger

logger = logging.getLogger('dev stats')
format_logger(logger)


def update_github_stats(data):
    logger.info('Getting development data')
    for row in data:
        update_github_stats_row(row)


def update_github_stats_row(row):
    logger.debug('Acquiring github data for %s' % (row['name']))
    if row.get('github_url') is None or row.get('github_url') == '':
        logger.warning("\tThere was no github_url for coin: {}".format(row['name']))
        return None

    coin_id = coin.get_coin_id_for_symbol(row['symbol'])
    if coin_id is None:
        logger.warning("Couldn't find coin_id for coin symbol: {}".format(row['symbol']))
        return None

    github.backfill_github_data(coin_id, row['github_url'])
