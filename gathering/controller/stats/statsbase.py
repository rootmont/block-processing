from common.db import timeseries, benchmarks
import datetime

from gathering.config import earliest_crypto_date, age_clusters, mcap_clusters, industry_clusters
from common.config import maybe_parallel


class StatsBase(object):

    @classmethod
    def cluster_to_cluster_type(cls, cluster):
        if cluster in age_clusters:
            return 'age'
        if cluster in mcap_clusters:
            return 'mcap'
        if cluster in industry_clusters:
            return 'industry'
        if cluster == 'global':
            return 'global'

    @classmethod
    def get_column_name(cls, x, rank=False):
        time, cluster, metric, name, value = x
        if rank:
            col = '%s_%s_rank_%s' % (metric, time, cluster)
        elif cluster is not None:
            col = '%s_%s_%s' % (metric, time, cluster)
        else:
            col = '%s_%s' % (metric, time)
        return col

    @classmethod
    def format_stats(cls, date, stats):
        raise NotImplementedError

    @classmethod
    def update(cls, date):
        cls.logger.debug('Getting %s stats for date %s' % (cls.stat_type, date))
        stats = cls.get_stats_for_date(end_date=date)
        formatted, bench = cls.format_stats(date, stats)

        cls.logger.info('Inserting %d %s stats into db for %s' % (len(formatted), cls.stat_type, date))
        cls.insert(formatted=formatted)
        cls.logger.info('Inserting %d %s stats into benchmarks for %s' % (len(bench), cls.stat_type, date))
        maybe_parallel(benchmarks.insert, bench, parallel=False)

    @classmethod
    def insert(cls, formatted):
        raise NotImplementedError

    @classmethod
    def backfill(cls, backfill_all=True):
        latest = timeseries.get_latest_usage_date()
        if latest is None or backfill_all:
            latest = earliest_crypto_date + datetime.timedelta(days=1)
        cls.logger.info('Updating stats from %s' % latest)
        today = datetime.date.today()
        days_back = (today - latest).days
        for i in range(days_back + 1):
            date = latest + datetime.timedelta(days=i)
            cls.update(date)

    # output is { time: { cluster: [stats, rank, pctile, summary] } }
    @classmethod
    def get_stats_for_date(cls, end_date):
        raise NotImplementedError

    # input should be dataframe with columns=coins and index=metrics
    @classmethod
    def get_distribution(cls,stats):
        # take rank down index, ranking coins against each other
        rank = stats.rank(axis='index', pct=True)

        # overall pctile is mean across all metrics
        pctile = rank.mean(axis='columns')

        # describe returns quantiles per column (metric in this case)
        summary = stats.describe().fillna(0)
        return rank, pctile, summary
