from common.db import coin, social as social_db
from gathering.controller.routines import social as social_routines
from gathering.config import age_clusters, mcap_clusters, industry_clusters
from common.config import format_logger, maybe_parallel
from .statsbase import StatsBase
import pandas as pd
import logging


class SocialStats(StatsBase):
    stat_type = 'social'
    logger = logging.getLogger('social stats')
    format_logger(logger)


    @classmethod
    def get_stats_for_date(cls, end_date):
        df = cls.get_social_stats(target_date=end_date)

        ret = {}

        # filter and pack it
        for age_cluster in age_clusters:
            coins = coin.get_coin_info_age(age_cluster)
            ret[age_cluster] = cls._filter(df, coins)
        for mcap_cluster in mcap_clusters:
            coins = coin.get_coin_info_marketcap(mcap_cluster)
            ret[mcap_cluster] = cls._filter(df, coins)
        for industry_cluster in industry_clusters:
            coins = coin.get_coin_info_industry(industry_cluster)
            ret[industry_cluster] = cls._filter(df, coins)
        coins = coin.get_coin_info_all()
        ret['global'] = cls._filter(df, coins)

        return ret


    @classmethod
    def insert(cls, formatted):
        maybe_parallel(social_db.insert, formatted.items(), parallel=False)


    @classmethod
    def get_column_name(cls, x, rank=False):
        # cluster_type, metric, coin, value
        cluster, metric, _, _ = x
        if rank:
            col = '%s_rank_%s' % (metric, cluster)
        else:
            col = metric
        return col


    @classmethod
    def get_social_stats(cls, target_date=None):
        cls.logger.info('Getting social data')
        twitter_df = social_routines.get_twitter_stats(target_date=target_date)
        reddit_df = social_routines.get_reddit_stats(target_date=target_date)
        social_df = pd.concat([reddit_df, twitter_df], axis='columns', sort=True).fillna(0)
        return social_df


    @classmethod
    def _filter(cls, stats, coins):
        # if you don't filter here, .loc will actually add the rows if they don't exist, and fill with nan (wtf)
        # .reindex might do what we want, see pandas warnings
        names = [x['token_name'] for x in coins if x['token_name'] in stats['week_twitter_engagement'].index]
        filtered = stats.loc[names]
        rank, pctile, summary = cls.get_distribution(filtered)
        return filtered, rank, pctile, summary


    @classmethod
    def get_hype_score(cls, social_df):
        social_cols = ['bitcointalk_posts', 'total_tweets', 'twitter_following', 'twitter_followers', 'twitter_leadership', 'reddit_members']
        combo_score = sum([pd.np.log(1 + social_df[col]) for col in social_cols])
        hype = combo_score / pd.np.log(2 + social_df['marketcap'])
        return hype


    @classmethod
    def format_stats(cls, date, stats):
        metric_args = []
        benchmark_args = []
        rank_args = []
        # stats =  { time: { cluster: [statz, rank, pctile, summary] } }
        for cluster, dd in stats.items():
            cluster_type = cls.cluster_to_cluster_type(cluster)
            statz, rank, percentile, summary = dd
            # first do metrics
            for metric in statz:
                smr = summary[metric]
                derived_metric = metric
                benchmark_args.append(
                    [date, cluster_type, cluster, derived_metric, smr['mean'], smr['std'], smr['min'], smr['25%'],
                     smr['50%'], smr['75%'], smr['max'], smr['count']])
                for coin in statz.index:
                    value = statz[metric][coin]
                    metric_args.append([None, metric, coin, value])
            # then ranks
            for metric in rank:
                for coin in rank.index:
                    value = rank[metric][coin]
                    rank_args.append([cluster_type, metric, coin, value])
            # then percentile
            for coin in percentile.index:
                rank_args.append(['global', cls.stat_type, coin, percentile.loc[coin]])
        organized = {x[-2]: {'date': date} for x in metric_args}
        for x in metric_args:
            col = cls.get_column_name(x, rank=False)
            name, value = x[-2:]
            organized[name][col] = value
        for x in rank_args:
            col = cls.get_column_name(x, rank=True)
            name, value = x[-2:]
            organized[name][col] = value

        return organized, benchmark_args
