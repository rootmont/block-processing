import datetime

import numpy as np
from sklearn.cluster import KMeans

from common.db import timeseries, coin
import logging
logger = logging.getLogger('gathering')


def cluster_ages():
    logger.info('Clustering coins by age')
    ages = timeseries.get_earliest_prices().to_frame('earliest_price')
    today = datetime.date.today()
    ages['age'] = ages['earliest_price'].apply(lambda x: (today  - x).days)
    ages['labels'] = get_k_clusters(ages['age'], k=3)
    assign_labels(ages, 'age', ['Emerging', 'Established', 'Mature'])

    return ages


def assign_labels(df, sort_column, string_labels):
    age_labels = [[i, df[df['labels'] == i].iloc[0][sort_column]] for i in range(len(string_labels))]
    sort = sorted(age_labels, key=lambda x: x[1])
    for i, lab in enumerate(sort):
        lab.append(string_labels[i])
    sort = sorted(sort, key=lambda x: x[0])
    string_labels = [sort[label][-1] for label in df['labels'].values]
    df['string_labels'] = string_labels


def cluster_coins():
    logger.info('Clustering coins')
    mcaps = cluster_marketcaps()
    ages = cluster_ages()
    for i in range(len(mcaps)):
        name = mcaps.iloc[i]['token_name']
        mcap_label = mcaps.iloc[i]['string_labels']
        if name not in ages.index:
            logger.warning('No age found for name %s' % name)
            continue
        age_label = ages.loc[name]['string_labels']
        logger.debug('Clusters for %s: %s %s' % (name, mcap_label, age_label))
        coin.insert_cluster_columns(name, mcap_label, age_label)


def cluster_marketcaps():
    logger.info('Clustering coins by marketcap')
    num_clusters = 4
    mcaps = timeseries.get_latest_marketcaps().fillna(1).replace(0, 1)
    if len(mcaps) < num_clusters:
        logger.error('db.get_latest_marketcaps() returned too few items to cluster')
        return mcaps
    mcaps['log_caps'] = np.log(mcaps['marketcap'])

    mcaps['labels'] = get_k_clusters(mcaps['log_caps'], k=4)
    assign_labels(mcaps, 'log_caps', ['Micro Cap', 'Small Cap', 'Medium Cap', 'Large Cap'])
    return mcaps


def get_k_clusters(data, k=3):
    model = KMeans(k)
    model.fit(data.values.reshape(-1, 1))
    return model.labels_
