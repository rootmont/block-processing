from common.db import timeseries, coin
from gathering.config import earliest_crypto_date, earliest_backfill_date
from common.config import maybe_retry, maybe_parallel
from sensors import nomics
import logging

logger = logging.getLogger('gathering')


def get_price_historic(name):
    ret = maybe_retry(nomics.get_price_nomics, [name])
    if ret is None:
        ret = []
    return ret


def get_prices(coin_data, parallel=True, backfill_all=False):
    logger.info('Getting prices')
    # earliest_crypto_date = datetime.date(2019,3,1)
    if backfill_all:
        args = [
            [[coin['symbol'], earliest_crypto_date]]
            for coin in coin_data
        ]
    else:
        latest = timeseries.get_latest_prices()
        nomics_coins = nomics.get_nomics_coins()
        args = [
            [[
                coin['symbol'],
                latest.get(coin['name'], earliest_backfill_date)
            ]]
            for coin in coin_data
            if coin['symbol'] in nomics_coins
        ]

    results = maybe_parallel(nomics.get_price_nomics, args, parallel)
    # get_price = {date:  {price_high, price_low, price_open, price_close, trading_volume }  }
    ret = {}
    for result in results:
        ret.update(result)
    return ret


def backfill_prices(args):
    sym, prices = args
    # reset connection to allow parallel writes to db via parallel connections
    coin_id = coin.get_coin_id_for_symbol(sym)
    for date in prices:
        logger.debug('Backfilling prices %s for symbol %s on date %s' % (prices[date], sym, date))
        d = prices[date]
        d['coin_id'] = coin_id
        d['date_string'] = date.strftime("%Y/%m/%d")
        timeseries.insert_price_2(**d)


def backfill_prices_retry(args):
    maybe_retry(backfill_prices, args)


def backfill_prices_all(prices, parallel=False):
    logger.info('Backfilling prices')
    maybe_parallel(backfill_prices_retry, prices.items(), parallel=parallel)


def update_prices(coin_data, parallel, backfill_all):
    logger.info('Updating prices')
    prices = get_prices(coin_data=coin_data, parallel=parallel, backfill_all=backfill_all)
    backfill_prices_all(prices, parallel=parallel)
