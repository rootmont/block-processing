from common.db import coin, timeseries

import pymysql

from common.config import maybe_retry, maybe_parallel, format_logger
from common.env import DEVENV
import logging

logger = logging.getLogger('gathering')

def update_coin_row(row):
    logger.debug('Processing %s' % row['name'])
    # reset connection bc we may be in a different thread
    if coin.coin_exists(row['name']):
        logger.debug('   Updating %s into coin table' % row['name'])
        try:
            coin.update_coin(
                company_name=row['name'],
                token_name=row['name'],
                symbol=row['symbol'],
                ico=row['ico'],
                mineable=row['mineable'],
                open_source=row['open_source'],
                ico_raise=row['ico_raise'],
                max_supply=row['max_supply'],
                hash_algorithm=row['hash_algorithm'],
                blockchain_type=row['blockchain_type'],
                disclosure=row['disclosure'],
                execution=row['execution'],
                potential=row['potential'],
                description=row['description'],
                website_url=row['website_url'],
                whitepaper_url=row['whitepaper_url'],
                twitter=row['twitter'],
                reddit=row['reddit'],
                telegram=row['telegram']
            )
        except pymysql.DataError as e:
            logger.error(e)
    else:
        logger.debug('   Inserting %s into coin table' % row['name'])
        try:
            coin.insert_coin(
                company_name=row['name'],
                token_name=row['name'],
                symbol=row['symbol'],
                ico=row['ico'],
                mineable=row['mineable'],
                open_source=row['open_source'],
                ico_raise=row['ico_raise'],
                max_supply=row['max_supply'],
                hash_algorithm=row['hash_algorithm'],
                blockchain_type=row['blockchain_type'],
                disclosure=row['disclosure'],
                execution=row['execution'],
                potential=row['potential'],
                description=row['description'],
                website_url=row['website_url'],
                whitepaper_url=row['whitepaper_url'],
                twitter=row['twitter'],
                reddit=row['reddit'],
                telegram=row['telegram']
            )
        except pymysql.DataError as e:
            logger.error(e)

    logger.debug('   Inserting %s into coin_category table for coin %s' % (row['industry'], row['name']))
    coin.insert_coin_industry(name=row['name'], industry=row['industry'])


def update_coin_table(data, parallel=True, delete_absent=False):
    logger.info('Updating coin table')
    maybe_parallel(update_coin_row, data, parallel)
    if delete_absent == False or DEVENV:
        return
    table_coins = coin.get_coin_info_all()
    for table_coin in table_coins:
        if len([x for x in data if x['name'] == table_coin['token_name']]) == 0:
            logger.warning('Deleting token %s since it is absent from the spreadsheet' % table_coin['token_name'])
            coin.delete_coin_name(table_coin['token_name'])


def delete_duplicate_coins():
    symbols = [x['ticker_symbol'] for x in coin.get_all_symbols()]
    for symbol in symbols:
        coin_ids = coin.get_all_coin_ids_for_symbol(symbol)
        if len(coin_ids) > 1:
            input('Deleting coin_ids %s for symbol %s Press enter to continue' % (coin_ids[1:], symbol))
            for coin_id in coin_ids[1:]:
                coin.delete_coin_id(coin_id['coin_id'])
