from common.db import timeseries, coin
from gathering.config import earliest_nomics_supply_date, earliest_crypto_date
from common.config import maybe_error, maybe_parallel
from sensors import nomics
import logging
import datetime
logger = logging.getLogger('gathering')


def update_marketcaps(coin_data, parallel, backfill_all):
    logger.info('Updating marketcaps and supplies')
    mcap_supplies = maybe_error(get_all_marketcaps_supplies, [(coin_data, parallel, backfill_all)])
    backfill_marketcaps_all(mcap_supplies, parallel)
    # mcaps90 = timeseries.get_avg_mcaps_days_back(90)


def backfill_marketcaps_all(mcap_supplies, parallel=False):
    logger.info('Backfilling marketcaps and supplies')
    maybe_parallel(backfill_marketcaps, mcap_supplies, parallel=parallel)


def backfill_marketcaps(args):
    date, mcaps, supplies = args
    for symbol in mcaps:
        datestring = date.strftime('%Y/%m/%d')
        logger.info('Inserting mcap: %s, %s, %d, %d' % (symbol, datestring, mcaps[symbol], supplies[symbol]))
        timeseries.insert_supply_mcap(symbol, datestring, mcaps[symbol], supplies[symbol])


def get_all_marketcaps_supplies(args):
    coin_data, parallel, backfill_all = args
    logger.info('Getting supplies')
    # supplies = { date: { currency: supply } }
    supplies = get_all_supplies_nomics(coin_data, parallel, backfill_all=backfill_all)
    mcaps = {}
    logger.info('Computing marketcaps from supplies')
    rootmont_symbols = {x['ticker_symbol']: True for x in coin.get_all_symbols()}
    args = [[rootmont_symbols, date, supply] for date, supply in supplies.items()]
    results = maybe_parallel(get_mcap_for_date, args, parallel, num_processes=4)
    for result in results:
        mcaps.update(result)
    return [(date, mcaps[date], supplies[date]) for date in mcaps]


def get_mcap_for_date(args):
    rootmont_symbols, date, supplies = args
    logger.debug('Computing marketcaps from supplies for date %s' % date)
    mcaps = {}
    # do this to reset the db connection so we don't stop on other threads
    prices = timeseries.get_all_prices_for_date(date)
    for symbol in supplies:
        #:D
        if not rootmont_symbols.get(symbol):
            continue
        logger.debug('Computing marketcaps from supplies for date %s symbol %s' % (date, symbol))
        supply = supplies.get(symbol, 0)
        price = prices.get(symbol, 0)
        mcap = price * supply
        if mcap == 0:
            continue
        try:
            mcaps[symbol] = int(mcap)
        except TypeError:
            msg = 'Failed to compute marketcap with supply %s price %s symbol %s' % (supply, price, symbol)
            logger.error(msg)
    return {date: mcaps}


def get_all_supplies_nomics(coin_data, parallel=True, backfill_all=False):
    """
    :param backfill_all: whether or not to backfill all nomics data
    :return: { date: { currency: supply } }
    """
    # latest[symbol] = date
    latest = timeseries.get_latest_supplies()
    coins_we_already_have = set(latest.index)
    coins_we_can_get = nomics.get_nomics_supply_coins()
    coins_we_want = {x['symbol'] for x in coin_data}
    missing_coins = (coins_we_want & coins_we_can_get) - coins_we_already_have
    # if we are trying to re-backfill everything, ignore the latest
    if backfill_all:
        days_back = (datetime.date.today() - earliest_nomics_supply_date).days
    # if we are missing some coins in the db, must go back to beginning
    elif len(missing_coins) > 0:
        logger.warning('Missing supplies for coins %s' % missing_coins)
        days_back = (datetime.date.today() - earliest_nomics_supply_date).days
    # else start at the earliest missing supply
    else:
        days_back = (datetime.date.today() - latest.filter(coins_we_can_get).min()).days
    logger.info('Getting supplies for a total of %d days back' % days_back)
    # each interval is 2 days (open & close), so we have a stride of 2
    # results = [ { date: { symbol: supply } } ]
    results = maybe_parallel(nomics.get_supply_nomics, range(0, days_back + 1, 2), parallel=parallel, num_processes=20)
    ret = {}
    for result in results:
        # dates may occur in more than one result!
        for date in result:
            if date not in ret: ret[date] = {}
            for symbol in result[date]:
                # filter here unless backfill_all is set
                if symbol not in coins_we_want: continue
                if backfill_all or date > latest.get(symbol, earliest_crypto_date):
                    ret[date][symbol] = result[date][symbol]
            # ret[date].update(result[date])
    return ret
