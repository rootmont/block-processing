import logging
from typing import List

from common.db import coin, current_stats
from sensors import nomics
from common.config import format_logger

logger = logging.getLogger('current stats')
format_logger(logger)


class CurrentStatsUpdateController(object):
    def __init__(self):
        self._coins = coin.get_all_coin_info()
        self._price_map = dict()

    # def update_prices(self):
    #     ps = nomics.get_current_prices()
    #     batch_list: List = []
    #     for price in ps:
    #         for c in self._coins:
    #             if price["currency"].lower() == c["ticker_symbol"].lower():
    #                 c_data = {
    #                     "coid_id": c["coin_id"],
    #                     "token_name": c["token_name"],
    #                     "price": float(price["price"])
    #                 }
    #                 batch_list.append(c_data)
    #                 logging.info("Inserting price data: {}, {}".format(price["currency"], float(price["price"])))
    #     prices.insert_update_batch(batch_list)
    #     pass

    def update_stats(self):
        stats = nomics.get_current_stats()
        batch_list: List = []
        for stat in stats:
            for c in self._coins:
                if stat["currency"].lower() == c["ticker_symbol"].lower():
                    for k, v in stat.items():
                        if v is None: stat[k] = 0
                    c_data = {
                        "coid_id": c["coin_id"],
                        "token_name": c["token_name"],
                        "price": float(stat.get("close", 0)),
                        "price_open": float(stat.get("dayOpen", 0)),
                        "volume": float(stat.get("dayVolume", 0)),
                        "supply": float(stat.get("availableSupply", 0))
                    }
                    c_data["marketcap"] = c_data["supply"] * c_data["price"]
                    batch_list.append(c_data)
                    logger.debug("Inserting current stats for: {}, {}".format(stat["currency"], float(stat["close"])))
        current_stats.insert_update_batch(batch_list)
