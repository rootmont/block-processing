from common.db import coin, reddit_db, social as social_db, twitter as twitter_db
import pandas as pd
from common.rank_tree import rank_tree

import datetime
import logging
logger = logging.getLogger('social')
# format_logger(logger)


def get_reddit_stats(target_date=None):
    reddit_data = reddit_db.get_all_reddit_objs()
    if target_date is None:
        target_date = datetime.date.today()
    df = reddit_db.get_df(reddit_data, target_date)
    return df

#TODO: make this more efficient, compute from previous day
def get_twitter_stats(target_date=None):
    raw = twitter_db.get_all()
    raw['total_engagement'] = raw['likes'] + raw['replies'] + raw['retweets'] + raw['tweets']
    df = raw.pivot(index='my_date', columns='token_name', values='total_engagement')
    df.set_index(pd.DatetimeIndex(df.index), inplace=True)
    today_eng = df.resample('D').sum()
    week_eng = today_eng.rolling(window=7, min_periods=1, closed='both', axis='index').sum() / 7
    total_eng = today_eng.rolling(window=100000000, min_periods=1, closed='both', axis='index').sum()

    first_date = today_eng.index[0]
    ind = pd.Series(index=total_eng.index,
                    data=[(x - first_date).days + 1 for x in total_eng.index])
    avg_daily_eng = total_eng.div(ind, axis='index')
    # total_eng_rank = total_eng.rank(pct=True, axis='columns')
    # avg_daily_eng_rank = avg_daily_eng.rank(pct=True, axis='columns')
    # week_eng_rank = week_eng.rank(pct=True, axis='columns')

    if target_date is None:
        target_date = datetime.date.today()
    if type(target_date) == datetime.date:
        target_date = pd.Timestamp(target_date)

    week = week_eng.loc[target_date]
    week.name = 'week_twitter_engagement'
    avg = avg_daily_eng.loc[target_date]
    avg.name = 'avg_daily_twitter_engagement'
    total = total_eng.loc[target_date]
    total.name = 'total_twitter_engagement'
    target = pd.concat([
        week,
        avg,
        total
    ], axis='columns')

    return target


def get_social_ranks_for_date(target_date=None):
    if target_date is None:
        df = social_db.get_latest_social_ranks()
    else:
        df = social_db.get_social_ranks_for_date(target_date=target_date)
    return df.mean(axis='columns')


def get_complete_social_stats(df: pd.DataFrame):
    df['hype'] = get_hype_score(df)
    filtered = [x['token_name'] for x in coin.get_coin_info_all()]
    df = df.reindex(filtered)
    current_cols = set(df.columns)
    desired_cols = set(rank_tree['root_rank']['social_rank'].values())
    if len(desired_cols - current_cols) > 0:
        logger.warning('Missing desired columns in social db {}'.format(desired_cols - current_cols))
    drop_cols = current_cols - desired_cols
    df.drop(columns=drop_cols, inplace=True)
    df.fillna(0, inplace=True)
    metric_rank = df.rank(pct=True, axis='index')
    social_rank = metric_rank.mean(axis='columns')
    return df, metric_rank, social_rank


def get_hype_score(social_df, inplace=False):
    social_cols = ['total_tweets', 'twitter_followers', 'twitter_leadership', 'reddit_members']
    combo_score = sum([pd.np.log(1 + social_df[col]) for col in social_cols])
    hype = combo_score / pd.np.log(2 + social_df['marketcap'])
    if inplace:
        social_df['hype'] = hype
    else:
        return hype

