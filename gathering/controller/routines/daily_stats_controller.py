import concurrent
import logging
from concurrent.futures import ThreadPoolExecutor
import pandas as pd
from common.config import format_logger
from common.db.connect import get_columns
from common.db.timeseries import get_latest_timeseries_raw, get_latest_price_metrics
from common.db.coin import get_all_coins_raw, get_all_clusters
from common.db.risk import get_latest_risk_raw
from common.db.daily_stats import insert_update_all, _get_catalog
from common.db.github_db import to_data_tables, get_for_all_coins
from common.db.ranks import get_latest_ranks
from common.db.social import get_latest_social_stats

logger = logging.getLogger('daily stats')
format_logger(logger)


class DailyStatsController(object):
    def __init__(self):
        logger.info("Updating Daily Stats")
        self._db_functions = [get_latest_risk_raw, get_latest_timeseries_raw, get_latest_ranks, get_latest_social_stats]
        self._exclusive_columns = ["id", "coin_id", "updated"]

        self._prices = get_latest_price_metrics().pivot(index='my_date', columns='token_name')
        # Get the columns for each of these tables
        self._daily_stats_cols = get_columns("daily_stats")
        logger.debug('Retrieving all dev stats')
        self._dev_stats_dfs = to_data_tables(get_for_all_coins())
        logger.debug('Retrieving all recent transactions')
        logger.debug('Getting cluster data')
        self._clusters_df = get_all_clusters()
        logger.debug('Getting non-timeseries data about coins')
        self._coins = get_all_coins_raw()
        self._daily_stats = {}
        logger.debug('Getting current catalog')
        self._catalog = {x['name']: x for x in _get_catalog()}

    def execute_futures(self, coin):
        with ThreadPoolExecutor(max_workers=len(self._db_functions)) as executor:
            futures = {executor.submit(func, coin["coin_id"]) for func in self._db_functions}
            for future in concurrent.futures.as_completed(futures):
                ret = future.result()
                if ret is not None:
                    self._append_results(coin["coin_id"], ret)
                else:
                    logger.warning("Couldn't find a timeseries, social, ranks or risk latest entry for: {}"
                                    .format(coin["token_name"]))
                    return False
        return True

    def update_stats(self):
        for coin in self._coins:
            # if coin['token_name'] not in ['Ethereum']: continue
            logger.debug('Updating stats for coin: {}'.format(coin['token_name']))
            if self.execute_futures(coin) is False:
                if coin['coin_id'] in self._daily_stats:
                    del(self._daily_stats[coin["coin_id"]])
                continue
            self._daily_stats[coin["coin_id"]]["coin_id"] = coin["coin_id"]
            self._daily_stats[coin["coin_id"]]["token_name"] = coin["token_name"]
            self._daily_stats[coin["coin_id"]]["ticker_symbol"] = coin["ticker_symbol"]
            self._append_dev_ranks(coin["coin_id"], coin["token_name"])
            self._append_clusters(coin["coin_id"])
            self._append_prices(coin["coin_id"])
            logger.info("Finished daily stats for {}, root_rank: {}".format(coin["token_name"], self._daily_stats[coin["coin_id"]]["root_rank"]))
        insert_update_all(self._daily_stats)

    def _append_prices(self, coin_id):
        name = self._daily_stats[coin_id]['token_name']
        prices = self._prices['price_close'].get(name, pd.DataFrame([]))
        mcaps = self._prices['marketcap'].get(name, pd.DataFrame([]))
        vols = self._prices['trading_volume'].get(name, pd.DataFrame([]))
        nonzero_prices = prices[prices > 0]
        nonzero_mcaps = mcaps[mcaps > 0]
        nonzero_vols = vols[vols > 0]
        mcap = float(nonzero_mcaps.iloc[-1]) if len(nonzero_mcaps) > 0 else None
        # make this step back two, because we have partial volumes being reported...
        vol = float(nonzero_vols.iloc[-2]) if len(nonzero_vols) > 1 else None
        price = float(nonzero_prices.iloc[-1]) if len(nonzero_prices) > 0 else None
        price_chng = float(nonzero_prices.pct_change().replace([pd.np.inf, pd.np.nan], 0).iloc[-1]) if len(nonzero_prices) > 0 else None
        logger.debug('Assigning to {} mcap: {}, vol: {}, price: {}, price change: {}'.format(name, mcap, vol, price, price_chng))
        self._daily_stats[coin_id]['marketcap'] = mcap
        self._daily_stats[coin_id]['trading_volume'] = vol
        self._daily_stats[coin_id]['price_close'] = price
        self._daily_stats[coin_id]['price_24h_pct'] = price_chng

    def _append_clusters(self, coin_id):
        name = self._daily_stats[coin_id]['token_name']
        self._daily_stats[coin_id].update(self._clusters_df.loc[name])

    def _append_results(self, coin_id, database_object):
        if self._daily_stats.get(coin_id) is None:
            self._daily_stats[coin_id] = {}
        for k in database_object:
            if k not in self._exclusive_columns and k in self._daily_stats_cols:
                self._daily_stats[coin_id][k] = database_object[k]

    def _append_dev_ranks(self, coin_id, token_name: str):
        logger.debug('Organizing dev stats for {}'.format(token_name))
        stats = self._dev_stats_dfs[0].loc[token_name]
        ranks = self._dev_stats_dfs[1].loc[token_name]
        dev_rank = self._dev_stats_dfs[2].loc[token_name]
        self._daily_stats[coin_id]["dev_rank"] = float(dev_rank)
        for col in stats.index:
            self._daily_stats[coin_id][col] = float(stats[col])
            self._daily_stats[coin_id]['%s_rank_global' % col] = float(ranks[col])
