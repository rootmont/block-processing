import os
import yaml
import argparse
import sys
import time
import logging
import datetime
from common.config import format_logger
from gathering.controller.stats import root as root_stats

logger = logging.getLogger('gathering')
format_logger(logger)


from flask import Flask
from flask_api import status
from flask import Response

sys.path.append(os.path.abspath(os.sep.join([".."])))

from daily_update import update
from controller.stats import usage, risk
from sensors.ethereum import stream_blocks as stream_eth_blocks
from apscheduler.schedulers.background import BackgroundScheduler
from sensors.eos import stream_blocks as stream_eos_blocks
from controller.routines.daily_stats_controller import DailyStatsController
from controller.routines.price_update import CurrentStatsUpdateController


Risk = risk.RiskStats()
Usage = usage.UsageStats()

app = Flask(__name__)
dir_path = os.path.dirname(os.path.realpath(__file__))

parser = argparse.ArgumentParser(description='Service to gather information regarding coins')
parser.add_argument('-p', default=False, type=lambda x: (str(x).lower() == 'true'))
args = parser.parse_args()

config = None
if args.p is not None and args.p:
    config = yaml.safe_load(open(os.path.join(dir_path, "config-prod.yml")))
else:
    config = yaml.safe_load(open(os.path.join(dir_path, "config-dev.yml")))

scheduler = None


@app.route('/')
def root():
    return 'I am a teapot.', 420


@app.route('/health',  methods=['GET'])
def health():
    if not scheduler.running or scheduler.get_jobs().__len__() == 0:
        return Response("{}", status=status.HTTP_500_INTERNAL_SERVER_ERROR, mimetype='application/json')
    return Response("{}", status=status.HTTP_200_OK, mimetype='application/json')


def market_minute():
    logger.info('computing market minute')
    end = datetime.date.today()
    start = end - datetime.timedelta(days=7)
    root_stats.update_root_rank(start)
    root_stats.update_root_rank(end)


now = datetime.datetime.now()
if __name__ == '__main__':
    scheduler = BackgroundScheduler()
    # If we are not debugging, only run once per day at the hour 23, minute 59
    if not bool(config['debug']):
        # don't use now because it will barely miss, so we add one minute
        scheduler.add_job(update, start_date=now + datetime.timedelta(minutes=1), trigger='interval', hours=24, max_instances=1)
        # scheduler.add_job(update, trigger='cron', hour='23', minute='59', max_instances=1)
        scheduler.add_job(lambda: DailyStatsController().update_stats(), start_date=now + datetime.timedelta(minutes=1), trigger='interval', minutes=60, max_instances=1)
        scheduler.add_job(lambda: CurrentStatsUpdateController().update_stats(), trigger='interval', seconds=10, max_instances=1)
        scheduler.add_job(stream_eth_blocks, trigger='interval', seconds=60, max_instances=1)
        scheduler.add_job(market_minute)
    else:
        # DailyStatsController().update_stats()
        # CurrentStatsUpdateController().update_stats()
        # scheduler.add_job(update, start_date=datetime.datetime.now() + datetime.timedelta(minutes=1), trigger='interval', hours=24, max_instances=1)
        scheduler.add_job(market_minute)

        # update()
        # DailyStatsController().update_stats()
    scheduler.start()
    # Wait for scheduler to start
    time.sleep(1)
    app.debug = bool(config['debug'])
    try:
        app.run(host=str(config['host']), port=int(config['port']))
    except Exception as e:
        scheduler.shutdown()

