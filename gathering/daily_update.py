from gathering.config import gsheet_url, delete_absent_coins, parallel, coin_logo_directory
from common.config import maybe_retry, format_logger
from controller import clusters
from controller.routines.mcap import update_marketcaps
from controller.routines.price import update_prices
from controller.routines.coin import update_coin_table
from controller.routines.ntx import update_ntxs
from controller.stats import usage, risk, root, social
from controller.stats.dev import update_github_stats
from sensors import gsheet, cmc
from common.env import DEVENV
import traceback
import sys
import datetime
import os
import logging

logger = logging.getLogger('daily update')
format_logger(logger)
# format_logger(logging.getLogger('gathering'))

backfill_all = {
    'prices': False,
    'mcaps': False,
    'ntxs': False,
    'socials': False,
    'devs': False,
    'ranks': False
}


def get_logos(coin_data):
    logger.info('Getting logos')
    missing_coins = [coin for coin in coin_data if logo_exists(coin['symbol']) is False]
    for coin in missing_coins:
        cmc.get_logo(coin)


def logo_exists(symbol):
    return os.path.isfile('%s/%s.png' % (coin_logo_directory, symbol))


def filter_coin_data(coin_data):
    if len(coin_data) == 0:
        raise Exception('No coin data from spreadsheet')
    if DEVENV:
        return [x for x in coin_data if x['name'] == 'Ethereum']
        # return [x for x in coin_data if x['symbol'] != 'ADA' and x['symbol'] != '1ST'][:5]
    return coin_data


def update():
    try:
        start = datetime.datetime.now()
        logger.info(
            "Beginning daily update at %s"
            % start.strftime("%Y/%m/%d %H:%M:%S")
        )
        # coin_data = [ { 'symbol', 'name',...} ]
        logger.info('Getting master spreadsheet')
        pre_filtered_coin_data = maybe_retry(gsheet.get_master_spreadsheet, gsheet_url)
        coin_data = filter_coin_data(pre_filtered_coin_data)

        logger.info('Updating ntxs. Backfill all = {}'.format(backfill_all['ntxs']))
        update_ntxs(coin_data, parallel, backfill_all=backfill_all['ntxs'])

        logger.info('Updating usage stats')
        Usage = usage.UsageStats()
        Usage.update(datetime.date.today())

        logger.info('Updating social stats. Backfill all = {}'.format(backfill_all['socials']))
        Social = social.SocialStats()
        Social.update(datetime.date.today())

        logger.info('Updating dev stats.')
        update_github_stats(coin_data)

        logger.info('Updating all ranks. Backfill all = {}'.format(backfill_all['ranks']))
        root.update_root_rank()

        # logos
        #get_logos(coin_data)

        end = datetime.datetime.now()
        logger.info("Completed daily update. Ended at %s" % (end.strftime("%Y/%m/%d %H:%M:%S")))
        logger.info("It took %s" % (end - start))
    except Exception as e:

        exc_type, exc_value, exc_traceback = sys.exc_info()
        msg = '%s:%s' % (e, repr(traceback.extract_tb(exc_traceback)))
        logger.fatal('Daily update failed.')
        logger.fatal(msg)

        print("error traceback: {}".format(e))

