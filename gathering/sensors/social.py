import twitter
import requests
import re
import logging
logger = logging.getLogger('gathering')


# import praw
# from telegram.client import Telegram
# from telethon import TelegramClient, sync

from common.env import *
from config import firefox_headers

twitter_api = twitter.Api(TWITTERCONSUMERKEY, TWITTERCONSUMERSECRET, TWITTERACCESSTOKEN, TWITTERACCESSSECRET)

# reddit = praw.Reddit(client_id=REDDITID,
#                      client_secret=REDDITSECRET,
#                      password=REDDITPASSWORD,
#                      user_agent=REDDITUSERAGENT,
#                      username=REDDITUSERNAME)

# telegram = TelegramClient('new_session', TELEGRAMID, TELEGRAMHASH)
# telegram.start()
# print(telegram.get_me().stringify())


def get_twitter_user(handle):
    logger.debug('Getting twitter info for {}'.format(handle))
    ret = {}
    try:
        user = twitter_api.GetUser(screen_name=handle, return_json=True)
        ret['tweets'] = user['statuses_count']
        #likes = user['favourites_count']
        ret['following'] = user['friends_count']
        ret['followers'] = user['followers_count']
        if ret['following'] == 0:
            ret['leadership'] = ret['followers']
        else:
            ret['leadership'] = ret['followers'] / ret['following']

    except Exception as e:
        logger.warning('Could not find twitter account for user "%s" %s' % (handle, e))
    return ret

#
# def get_telegram():
#     tg = Telegram(
#         api_id=TELEGRAMID,
#         api_hash=TELEGRAMHASH,
#         phone=TELEGRAMPHONE,
#         database_encryption_key=TELEGRAMDBPW
#     )
#     # you must call login method before others
#     tg.login()
#
#     response = tg.get_chat_history(
#         chat_id='zz',
#         limit=1000,
#         from_message_id=0
#     )
#     response.wait()
#     print(response)




