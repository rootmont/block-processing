import datetime
import json
import requests
import dateutil

from common.env import NOMICSKEY
from common.config import maybe_retry
from gathering.config import nomics_date_format
import logging
logger = logging.getLogger('gathering')
current_stats_logger = logging.getLogger('current stats')

def parse_nomics_timestamp(datum):
    if type(datum) is str: datum = {'timestamp': datum}
    dt = dateutil.parser.parse(datum['timestamp'])
    return (dt + datetime.timedelta(hours=12)).date()
    # dt = datetime.datetime.strptime(datum['timestamp'].split('T')[0], nomics_date_format)
    # return datetime.date(dt.year, dt.month, dt.day)


def get_supply_nomics(days_back):
    return maybe_retry(supply_nomics, [days_back])


def supply_nomics(days_back):
    logger.info('Getting supplies from nomics for %d days back' % days_back)
    start_date = datetime.date.today() - datetime.timedelta(days_back+1)
    end_date = datetime.date.today() - datetime.timedelta(days_back)
    start_date_str = start_date.strftime(nomics_date_format) + 'T00:00:00Z'
    end_date_str = end_date.strftime(nomics_date_format) + 'T00:00:00Z'
    params = {
        'key': NOMICSKEY,
        'start': start_date_str,
        'end': end_date_str
    }
    url = 'https://api.nomics.com/v1/'
    path = 'supplies/interval'
    response = requests.get(url=url + path, params=params)
    if response.status_code == 401:
        raise LookupError('Unable to access nomics api: %s' % (url + path))
    data = json.loads(response.text)
    ret = {}

    for x in data:
        if x['open_timestamp'] is not None and x['open_available'] is not None:
            open_date = parse_nomics_timestamp(x['open_timestamp'])
            if open_date not in ret:
                ret[open_date] = {}
            ret[open_date][x['currency']] = int(float(x['open_available']))
        if x['close_timestamp'] is not None and x['close_available'] is not None:
            close_date = parse_nomics_timestamp(x['close_timestamp']) + datetime.timedelta(1)
            if close_date not in ret:
                ret[close_date] = {}
            ret[close_date][x['currency']] = int(float(x['close_available']))
    return ret
    # return {
    #     start_date: {
    #         x['currency']: int(float(x['open_available']))
    #         for x in data
    #         if x['open_available'] is not None
    #     },
    #     end_date: {
    #         x['currency']: int(float(x['close_available']))
    #         for x in data
    #         if x['close_available'] is not None
    #     }
    # }


def get_nomics_coins():
    params = {'key': NOMICSKEY}
    url = 'https://api.nomics.com/v1/currencies'
    response = requests.get(url=url, params=params)
    if response.status_code == 401:
        raise LookupError('Unable to access nomics api: %s' % url)
    data = json.loads(response.text)
    return {x['id'] for x in data}


def get_nomics_supply_coins():
    supplies = supply_nomics(1)
    date = list(supplies.keys())[0]
    return set(supplies[date].keys())


def get_price_nomics(args):
    return maybe_retry(price_nomics, args)


def price_nomics(args):
    symbol, start_date = args
    logger.debug('Getting price for %s from date %s' % (symbol, start_date))
    start_date_str = start_date.strftime(nomics_date_format) + 'T00:00:00Z'
    end_date_str = datetime.date.today().strftime(nomics_date_format) + 'T00:00:00Z'
    params = {
        'key': NOMICSKEY,
        'interval': '1d',
        'currency': symbol,
        'start': start_date_str,
        'end': end_date_str
    }
    url = 'https://api.nomics.com/v1/'
    path = 'candles'
    response = requests.get(url=url + path, params=params)
    if response.status_code == 401:
        raise LookupError('Unable to access nomics api: %s' % (url + path))
    data = json.loads(response.text)
    # get_price = {date:  {price_high, price_low, price_open, price_close, trading_volume }  }
    prices = {
        parse_nomics_timestamp(x) : {
            'price_high': x['high'],
            'price_low': x['low'],
            'price_open': x['open'],
            'price_close': x['close'],
            'trading_volume': x['volume']
        }
        for x in data
    }

    return {symbol: prices}


def get_current_prices():
    current_stats_logger.debug('Getting current prices')
    url = 'https://api.nomics.com/v1/prices?key=%s' % NOMICSKEY
    response = requests.get(url=url)
    if response.status_code == 401:
        raise LookupError('Unable to access nomics api: %s' % (url))
    data = json.loads(response.text)
    return data


def get_current_stats():
    current_stats_logger.debug('Getting current stats')
    url = 'https://api.nomics.com/v1/dashboard?key=%s' % NOMICSKEY
    response = requests.get(url=url)
    if response.status_code == 401:
        raise LookupError('Unable to access nomics api: %s' % (url))
    data = json.loads(response.text)
    return data
