from dateutil import parser as dateparser
from common.db import blocks, timeseries
from common.config import maybe_request, maybe, format_logger
from gathering.config import earliest_eos_date, request_args
from sensors import xfers
import requests
import json
import logging
import datetime
from time import sleep

logger = logging.getLogger('eos block stream')
format_logger(logger)


def get_chain_height():
    url = 'https://eos.greymass.com/v1/chain/get_info'
    txt = maybe_request([url, request_args], stall=0)
    if txt is None:
        logger.error('Could not get num eos blocks')
        return None
    j = json.loads(txt)
    num_blocks = j['last_irreversible_block_num']
    return num_blocks


def get_block(blocknum):
    payload = json.dumps({'block_num_or_id': str(blocknum)})
    res = requests.post('https://eos.greymass.com/v1/chain/get_block', data=payload)
    if res.status_code != 200:
        return None
    data = json.loads(res.text)
    return {
        'txs': len(data['transactions']),
        'blocknumber': blocknum,
        'datetime': dateparser.parse(data['timestamp'])
    }


def stream_blocks(start_block=0, sleep_time=5):
    from controller.routines.ntx import backfill_ntx_retry
    if start_block == 0:
        last_block_collected = blocks.get_latest_block('eos')
        if last_block_collected is None: last_block_collected = 1
    else:
        last_block_collected = start_block

    while True:
        tip = get_chain_height()
        if tip is None: continue
        if tip >= last_block_collected:
            block = get_block(last_block_collected)
            if block is None: continue
            logger.info('Inserting eos block %s' % (block))
            blocks.insert_block(blocknumber=block['blocknumber'], blockchain='eos', datetime=block['datetime'],
                                txs=block['txs'])
            last_block_collected += 1
        else:
            latest = timeseries.get_latest_ntxs()
            latest_date = latest.get('EOS', earliest_eos_date + datetime.timedelta(days=1))
            ntxs = {'EOS': xfers.get_ntx_from_db(latest_date - datetime.timedelta(days=1), 'eos')}
            logger.info('Backfilling ntxs from %s' % latest_date)
            maybe(backfill_ntx_retry, ntxs.items(), num_processes=1)
            logger.info('Sleeping for %d seconds' % sleep_time)
            sleep(sleep_time)
