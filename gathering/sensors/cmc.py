import json
import requests

from config import coin_logo_directory
import logging

cmc_listings = None


def get_all_cmc_listings():
    global cmc_listings
    if cmc_listings is None:
        url = 'https://api.coinmarketcap.com/v2/listings/'
        cmc_listings = json.loads(requests.get(url).text)
    return cmc_listings


def get_logo(coin):
    try:
        symbol = coin['symbol']
        name = coin['name']
        listings = get_all_cmc_listings()
        idee = [
            x['id']
            for x in listings['data']
            if x['name'].lower() == name.lower()
            or x['symbol'].lower() == symbol.lower()
        ][0]

        url = "https://s2.coinmarketcap.com/static/img/coins/128x128/%d.png" % idee
        image = requests.get(url).content
        with open("%s/%s.png" % (coin_logo_directory, symbol), "wb") as f:
            f.write(image)
            logging.info("Retrieved and saved logo for %s" % symbol)
    except Exception as e:
        logging.error("Failed to get logo for %s %s" % (coin, e))
