#!/usr/bin/env bash
PYTHONPATH=.:./common python3 ./test/presentation/api_test.py $1 $2
PYTHONPATH=. python3 ./test/presentation/consistency.py $1
#PYTHONPATH=. python3 -m pytest --durations=5 test/
